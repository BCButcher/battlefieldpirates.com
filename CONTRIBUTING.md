# Contributing

Merge Requests and Issues are very welcome on this repository, but please review this document first.

## Content Additions

When suggesting content additions, please describe what type of content it is &mdash; text, images, audio, video &mdash; and what it relates to &mdash; a Game, Mod, Source, Map, Page on the website. It is also very helpful if you can link to the where the content can be found online &mdash; if you have an old URL, check the [Wayback Machine](https://web.archive.org/).

If you're already familiar with the website's structure and content, feel free to propose a merge request.

### Metadata

The website's pages use distinct metadata in YAML FrontMatter to relate to each other taxonomically. For example the map [Azure Seas](https://gitlab.com/BCButcher/battlefieldpirates.com/-/blob/main/content/battlefield-pirates/maps/original/azure-seas/index.md):

```yaml
---
title: Azure Seas
date: 2005-11-01
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - BFP1 Final 1.0
type: map
---
```

**Title**: Every singular Page has a standalone title
**Date**: When the content was originally published
**Categories**: Relates the Page to a Game, Mod, or Source

- Game is `Battlefield 1942` or `Battlefield 2`
- Source is typically `Original` if the map shipped with the original Mod, or less typically something like `Anniversary` if it came in a coherent map pack, or `Custom` if it was released singularely

**Tags**: Relates the Page to a game type

- Typically `Conquest`, `CTF`, and the like, or specialized types like `Zombie`

**Labels**: Relates the Page to a release version

- Differentiated within mods by their prefix, such as `BFP1` for Battlefield Pirates, `BFP2`for Battlefield Pirates 2
- Follows the format `PREFIX TITLE VERSION`, such as `BFP1 Final 1.0`

**Type**: Codeword for the content type

## Bugs

A bug is a _demonstrable problem_ that is caused by the code in the repository.

Guidelines for bug reports:

1. Use the **[issue search](https://gitlab.com/BCButcher/battlefieldpirates.com/-/issues)** &mdash; check if the issue has already been reported

1. Check if the issue is already being solved in a **[Merge request](https://gitlab.com/BCButcher/battlefieldpirates.com/-/merge_requests)**

1. Create a [reduced test case](http://css-tricks.com/reduced-test-cases/) and **provide step-by-step instructions on how to recreate the problem**: Include code samples, Page snippets or YAML-configuration like settings or FrontMatter as needed

A good bug report shouldn't leave out information. Please try to be as detailed as possible in your report.

- What is your environment? Is it localhost, OSX, Linux, on a remote server? Is the same thing happening locally or on the remote server, or just on Windows?

- What steps will reproduce the issue? What browser(s) and OS' experience the problem?

- What would you expect to be the outcome?

- Did the problem start happening recently &mdash; e.g. after updating to a new version of something &mdash; or was this always a problem?

- Can you reliably reproduce the issue? If not, provide details about how often the problem happens and under which conditions it normally happens.

All these details will help to fix any potential bugs.

**Important**: [include Code Samples in triple backticks](https://help.github.com/articles/github-flavored-markdown/#fenced-code-blocks) so that code is indented properly. [Add the language name after the backticks](https://help.github.com/articles/github-flavored-markdown/#syntax-highlighting) to add syntax highlighting to the code snippets.

## Feature requests

Feature requests are welcome, but take a moment to find out whether your idea fits with the scope and aims of the project. It's up to _you_ to make a strong case to convince the project's developers of the merits of this feature. Please provide as much detail and context as possible.

## Merge requests

Good merge requests - patches, improvements, new features - are a fantastic help. They should remain focused in scope and not contain unrelated commits. They must also adhere to the current code standards.

**IMPORTANT**: By submitting a patch, you agree to allow the project owner to
license your work under the same license as that used by the project.
