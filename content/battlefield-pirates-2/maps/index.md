---
title: Pirates 2 Maps
date: 2008-12-19
description: Battlefield Pirates 2 featured 16 maps, with Conquest, Zombie, and Capture The Flag game modes. It was released as an open beta on September 20th 2007. Release 1 showcased the many weapons and kits available thru a series of infantry-based maps. Release 2, from December 18th 2008, provided a deeper  experience with more naval combat maps, new ships, and even aircraft. It also included Capture The Flag and Zombie modes.
categories:
  - Battlefield 2
type: mod
layout: layouts/maps.njk
---
