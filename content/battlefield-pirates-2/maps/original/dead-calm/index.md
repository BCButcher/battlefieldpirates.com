---
title: Dead Calm
author: 1/2Hawk
description: >-
  Right in the midst of a most epic naval battle between Undead and Peglegs, the
  wind dies! Prepare yourselves for an epic close quarters battle between the
  decks of two massive ship o' the lines!

  Dead Calm features two stationary and parallel ships-of-the-line anchored very
  close to each other in the middle of the sea. Albeit simple, Dead Calm
  provides versatile gameplay; it offers close-quarters swashbuckling on decks
  as well as long-distance sniping between the lookout posts on the masts. Dead
  Calm requires agility, fast reactions, and good aiming -- in many ways it is
  the perfect map for training as it offers both simplicity as well as
  versatility. Despite being especially popular amongst newcomers, Dead Calm is
  surely in the hearts of many veterans as well.
date: "2007-09-20"
categories:
  - Battlefield 2
  - Original
tags:
  - Conquest
labels:
  - BFP2 Release 1
map:
  gametypes:
    - Conquest
  sizes: []
  source: original
  controlpoints: []
  created: "2007-09-20"
  creator: 1/2Hawk
type: map
layout: layouts/map.njk
origin: >-
  https://web.archive.org/web/20090308132741/http://bfpirates.com/wiki/index.php/BFP2_Dead_Calm
---
