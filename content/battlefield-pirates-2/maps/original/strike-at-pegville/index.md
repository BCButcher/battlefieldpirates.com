---
title: Strike At Pegville
author: Bensta
description: ""
date: "2010-04-19"
categories:
  - Battlefield 2
  - Original
tags:
  - Conquest
  - Zombie
labels:
  - BFP2 Release 2.5
map:
  gametypes:
    - Conquest
    - Zombie
  sizes: []
  source: original
  controlpoints: []
  created: "2010-04-19"
  creator: Bensta
type: map
layout: layouts/map.njk
origin: >-
  https://web.archive.org/web/20231216155612/http://www.bfeditor.org/forums/index.php?/topic/14334-strike-at-pegville/
---
