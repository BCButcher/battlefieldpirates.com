---
title: Sailor's Warning
author: 1/2Hawk
description: >-
  I know what lies beneath, Ive seen the flash of teeth, conspiring with the
  reef to sink our ship. The wind's a cheating wife, her tongue a thirsty knife,
  and she could take your life with one good kiss. Can you see the sky turn red,
  as mornings light breaks over me. Know tonight we'll make our bed at the
  bottom of the sea... Look and see the sky turn red, like blood it covers over
  me, and soon the sea shall give up her dead. We'll raise an empire - from the
  bottom of the sea...
date: "2008-12-18"
categories:
  - Battlefield 2
  - Original
tags:
  - Conquest
labels:
  - BFP2 Release 2.1
map:
  gametypes:
    - Conquest
  sizes: []
  source: original
  controlpoints: []
  created: "2008-12-18"
  creator: 1/2Hawk
type: map
layout: layouts/map.njk
origin: >-
  https://web.archive.org/web/20081004111406/http://bfpirates.com/wiki/index.php/Sailors%27_Warning
---
