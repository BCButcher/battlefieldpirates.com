---
title: Crossbones Keep
author: 1/2Hawk
description: >-
  Feared and avoided by the locals as a haunted isle, a small fortress on this
  uniquely shaped outcropping has become a den for the Undead. Protected by a
  sheer cliff faces and precarious rocky pathways, some rather inventive pirates
  have brought a few unique vehicles to assault this keep from the air in order
  to claim it for themselves.
date: "2008-12-18"
categories:
  - Battlefield 2
  - Original
tags:
  - Conquest
  - Zombie
labels:
  - BFP2 Release 2.1
map:
  gametypes:
    - Conquest
    - Zombie
  sizes: []
  source: original
  controlpoints: []
  created: "2008-12-18"
  creator: 1/2Hawk
type: map
layout: layouts/map.njk
origin: >-
  https://web.archive.org/web/20081004111346/http://bfpirates.com/wiki/index.php/Crossbones_Keep
---
