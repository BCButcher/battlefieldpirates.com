---
title: O Me Hearty Beach
author: Bensta
description: ""
date: "2008-11-08"
categories:
  - Battlefield 2
  - Original
tags:
  - Conquest
  - Zombie
labels:
  - BFP2 Release 2.1
map:
  gametypes:
    - Conquest
    - Zombie
  sizes: []
  source: original
  controlpoints: []
  created: "2008-11-08"
  creator: Bensta
type: map
layout: layouts/map.njk
origin: >-
  https://web.archive.org/web/20231217150138/http://www.bfeditor.org/forums/index.php?/topic/12497-omaha-beach-remake-for-bfp2/#comment-83053
---
