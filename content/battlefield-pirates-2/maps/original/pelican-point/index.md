---
title: Pelican Point
author: Catbox
description: >-
  After a long voyage, the Pegleg's have come across a deserted lighthouse
  amongst a crop of tropical islands. Eager to set foot on land and acquire the
  outpost, the Pegleg's give little thought to where the previous inhabitants
  have gone.
date: "2007-09-20"
categories:
  - Battlefield 2
  - Original
tags:
  - Conquest
  - CTF
labels:
  - BFP2 Release 1
map:
  gametypes:
    - Conquest
    - CTF
  sizes: []
  source: original
  controlpoints: []
  created: "2007-09-20"
  creator: Catbox
type: map
layout: layouts/map.njk
origin: >-
  https://web.archive.org/web/20081004111355/http://bfpirates.com/wiki/index.php/Pelican_Point
---
