---
title: Plunders Island
author: Junglist
description: >-
  Test Version 0.1 | 64 Player only this build | Please post issues at BSS
  Forums | More ambients to come, 16 and 32 Player levels etc.
date: ""
categories:
  - Battlefield 2
  - Original
tags:
  - Conquest
labels:
  - BFP2 Release 2.5
map:
  gametypes:
    - Conquest
  sizes: []
  source: original
  controlpoints: []
  creator: Junglist
type: map
layout: layouts/map.njk
---
