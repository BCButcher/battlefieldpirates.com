---
title: Harbor
author: ""
description: ""
date: ""
categories:
  - Battlefield 2
  - Original
tags:
  - Conquest
  - CTF
  - Zombie
labels:
  - BFP2 Release 2.5
map:
  gametypes:
    - Conquest
    - CTF
    - Zombie
  sizes: []
  source: original
  controlpoints: []
type: map
layout: layouts/map.njk
---
