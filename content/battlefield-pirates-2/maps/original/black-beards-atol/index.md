---
title: Black Beards Atol
author: Catbox
description: >-
  Both pirate teams have come in search of the very spot where Black Beard is
  rumored to of buried his massive fortune. Secure the Atoll and kill those who
  seak to steal your booty!
date: "2007-09-20"
categories:
  - Battlefield 2
  - Original
tags:
  - Conquest
  - CTF
labels:
  - BFP2 Release 1
map:
  gametypes:
    - Conquest
    - CTF
  sizes: []
  source: original
  controlpoints: []
  created: "2007-09-20"
  creator: Catbox
type: map
layout: layouts/map.njk
origin: >-
  https://web.archive.org/web/20090308131622/http://bfpirates.com/wiki/index.php/Black_Beard's_Atol
---
