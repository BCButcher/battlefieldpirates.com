---
title: Stranded
author: 1/2Hawk
description: ""
date: "2008-12-18"
categories:
  - Battlefield 2
  - Original
tags:
  - Conquest
  - CTF
labels:
  - BFP2 Release 2.1
map:
  gametypes:
    - Conquest
    - CTF
  sizes: []
  source: original
  controlpoints: []
  created: "2008-12-18"
  creator: 1/2Hawk
type: map
layout: layouts/map.njk
origin: >-
  https://web.archive.org/web/20071012022220/http://bfpirates.com/phpBB2/viewtopic.php?t=1456
---
