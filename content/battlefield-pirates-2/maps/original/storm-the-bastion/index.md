---
title: Storm The Bastion
author: 1/2Hawk
description: >-
  After running off the imperial powers that built this imposing defensive
  complex known as the Bastion, a large Pirate crew has set up operations in the
  surrounding areas as a home base for plundering this sector of the Caribbean.
  Knowing little of the fort's long history of the brutal treatment and gruesome
  slaying of imprisoned pirates, they are as yet unaware of the large population
  of Undead lost souls who haunt nearby islands. Hoping to catch the Bastion
  guards asleep at the cannons, the Undead have launched an early morning raid
  by slipping their boats quietly into the fog ... hoping to successfully Storm
  the Bastion for themselves.
date: "2007-09-20"
categories:
  - Battlefield 2
  - Original
tags:
  - Conquest
  - Zombie
labels:
  - BFP2 Release 1
map:
  gametypes:
    - Conquest
    - Zombie
  sizes: []
  source: original
  controlpoints: []
  created: "2007-09-20"
  creator: 1/2Hawk
type: map
layout: layouts/map.njk
origin: >-
  https://web.archive.org/web/20081030071300/http://bfpirates.com/wiki/index.php/Storm_the_Bastion
---
