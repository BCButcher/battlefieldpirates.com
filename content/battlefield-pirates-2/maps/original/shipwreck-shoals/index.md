---
title: Shipwreck Shoals
author: 1/2Hawk
description: >-
  This stormy island once held one of the regions most fertile grounds for
  planting sugar. However, its rocky shoreline, frequent squalls and even more
  notorious reputation for smashing up cargo ships has earned it the nickname
  Shipwreck Shoals. Local pirates have recently discovered a large underground
  cavern here and will soon discover that other crews nearby still lurk about
  protecting their wares.
date: "2008-12-18"
categories:
  - Battlefield 2
  - Original
tags:
  - Conquest
  - CTF
labels:
  - BFP2 Release 2.1
map:
  gametypes:
    - Conquest
    - CTF
  sizes: []
  source: original
  controlpoints: []
  created: "2008-12-18"
  creator: 1/2Hawk
type: map
layout: layouts/map.njk
origin: >-
  https://web.archive.org/web/20081004111415/http://bfpirates.com/wiki/index.php/Shipwreck_Shoals
---
