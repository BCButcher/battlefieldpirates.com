---
title: Shallow Draft
author: Brother Justice
description: >-
  As the unpredictable deadly storms march through the region this cold season,
  two crews find themselves stranded. As the storm begins to clear and the night
  awakens with the presence of the wicked full moon the Peglegs and the Undead
  must battle each other to survive. Control the precious resources that other
  wayward crews left here long ago and use all means necessary to survive.
date: "2007-09-20"
categories:
  - Battlefield 2
  - Original
tags:
  - Conquest
  - CTF
labels:
  - BFP2 Release 1
map:
  gametypes:
    - Conquest
    - CTF
  sizes: []
  source: original
  controlpoints: []
  created: "2007-09-20"
  creator: Brother Justice
type: map
layout: layouts/map.njk
origin: >-
  https://web.archive.org/web/20081004111410/http://bfpirates.com/wiki/index.php/Shallow_Draft
---
