---
title: Battlefield Pirates 2 Original
date: 2008-12-19
description: Battlefield Pirates 2 featured 16 maps, with Conquest, Zombie, and Capture The Flag game modes. It was released as an open beta on September 20th 2007. Version 1 showcased the many weapons and kits available thru a series of infantry-based maps. Version 2.1, from December 18th 2008, provided a deeper  experience with more naval combat maps, new ships, and even aircraft. It also included Capture The Flag and Zombie modes.
categories:
  - Pirates 2
type: source
layout: layouts/source.njk
origin: >-
  https://web.archive.org/web/20090126005944/http://bfpirates.com:80/wiki/index.php/BFP2
---
