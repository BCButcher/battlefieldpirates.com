---
title: Pirates 2 Weapons
categories:
  - Battlefield 2
  - Pirates 2
type: mod
layout: layouts/details.njk
grid: "halves thirds"
details:
  - name: Handheld Weapons
    icon: musket.svg
    items:
      - name: Cutlass
        link: ../galleries/weapons/#cutlass
        image:
          - img/game/bfp2/weapons/handheld/cutlass/main1.png
          - img/game/bfp2/weapons/handheld/cutlass/main2.png
        description: "The Cutlass which is by far the most important weapon any Pirate can have. The cutlass is standard for all classes as it was the primary sword at the time. Not only that, but ye may never know when ye need to clash with another blade. The Cutlass is made only of the finest Spanish steel and because of this, it isn't likely for it to break."
      - name: Pistol
        link: ../galleries/weapons/#pistol
        image: img/game/bfp2/weapons/handheld/pistol/main.png
        description: "The (Flintlock) Pistol is used by the Raider, Gunner, and Surgeon classes. Unlike the pistolette, this weapon is reloadable in addition to being more accurate and capable of inflicting more damage."
      - name: Pistolette / French Pistol
        link: ../galleries/weapons/#pistolette-french-pistol
        image:
          - img/game/bfp2/weapons/handheld/pistolette/main1.png
          - img/game/bfp2/weapons/handheld/pistolette/main2.png
        description: "The Pistolettes are drawn from the players bandolier, shot and discarded quickly. The Pistolet is a single shot, non reload-able pistol which is somewhat inaccurate and will take some practice dueling to be used effectively against your enemies!"
      - name: Ducksfoot Pistol
        link: ../galleries/weapons/#ducksfoot-pistol
        image: img/game/bfp2/weapons/handheld/ducksfoot/main.png
        points:
          - 5 Bullets Per Shot
      - name: Blunderbuss
        link: ../galleries/weapons/#blunderbuss
        image: img/game/bfp2/weapons/handheld/blunderbuss/main.png
        description: "A blunderbuss could be filled with small rocks, glass shards, or nails, but more ideal was a handful of coarse shot or musket balls in order to not damage the barrel. To be most effective with the blunderbuss, aim for your opponent’s chest and be sure to make the majority of it's spread hit."
      - name: Musketoon
        link: ../galleries/weapons/#musketoon
        image: img/game/bfp2/weapons/handheld/musketoon/main.png
        description: "The Musketoon is a short barrel musket. The Musketoon is equipped with a small iron sight, and while it has a shorter range then the musket, it is quicker to reload, thus deadlier at mid-range to those who become skilled with it."
      - name: Musket
        link: ../galleries/weapons/#musket
        image: img/game/bfp2/weapons/handheld/musket/main.png
        description: "The Musket was the main standard weapon of the time used by pirates for its range, durability and it's easier use."
      - name: Sharpshooter’s Musket
        link: ../galleries/weapons/#sharpshooter%E2%80%99s-musket
        image: img/game/bfp2/weapons/handheld/sharpshooters_musket/main.png
        description: "The Sharpshooter’s Musket has the same accuracy as the musket, but the attached scope will provide a much higher zoom as opposed to looking down the barrel of the musket. It has a slightly slower reload time then the standard BFP2 Musket, but more ammo is available."
      - name: Grenade
        link: ../galleries/weapons/#grenade
        image: img/game/bfp2/weapons/utility/grenade/main.png
      - name: Grenade Launcher
        link: ../galleries/weapons/#grenade-launcher
        image: img/game/bfp2/weapons/handheld/grenade_launcher/main.png
  - name: Utilities
    icon: spyglass.svg
    items:
      - name: Smoke Grenade
        link: ../galleries/weapons/#smoke-grenade
        image: img/game/bfp2/weapons/utility/smoke_jar/main.png
      - name: Flaming Potionjar
        image: img/game/bfp2/weapons/utility/flaming_potion_jar/main.png
      - name: Powder Keg
        link: ../galleries/weapons/#powder-keg
        image: img/game/bfp2/weapons/utility/powder_keg/main.png
      - name: Crab
        link: ../galleries/weapons/#crab
        image: img/game/bfp2/weapons/utility/crab/main.png
      - name: Parrot
        image: img/game/bfp2/weapons/utility/parrot/main.png
      - name: Flare Launcher
        image: img/game/bfp2/weapons/utility/flare_launcher/main.png
      - name: Grappling Hook
        image: img/game/bfp2/weapons/utility/grappling_hook/main.png
      - name: Zipline Crossbow
        image: img/game/bfp2/weapons/utility/zipline_crossbow/main.png
      - name: Hammer
        link: ../galleries/weapons/#hammer
        image: img/game/bfp2/weapons/utility/hammer/main.png
      - name: Spyglass
        link: ../galleries/weapons/#spyglass
        image: img/game/bfp2/weapons/utility/spyglass/main.png
        description: "The Spy Glass, or monocular, is available to all classes and allows ye to spot enemies on the horizon, and alert your crew to approaching threats."
      - name: Ammunition Bag
        image: img/game/bfp2/weapons/utility/ammunition_bag/main.png
      - name: Surgeon Toolkit and Grog Keg
        link: ../galleries/weapons/#surgeon-toolkit-and-grog-keg
        image:
          - img/game/bfp2/weapons/utility/surgeon_toolkit/main.png
          - img/game/bfp2/weapons/utility/grog_keg/main.png
  - name: Cannons
    icon: cannon.svg
    items:
      - name: Field Cannon
        image: img/game/bfp2/weapons/utility/field_cannon/main.png
      - name: Falconet
        link: ../galleries/weapons/#falconet
        image:
          - img/game/bfp2/stationary/falconet/main.png
          - img/game/bfp2/stationary/falconet/alternative.png
      - name: 12-pounder
        link: ../galleries/weapons/#12-pounder
        image: img/game/bfp2/stationary/12pdr/main.png
        description: "The Pegleg's 12lb is a bit smaller and weaker than the 24lb but still packs quite a punch. However, both the 24lb Cannon and the 12lb Cannon are unique as they fire three type of ammunition of your choice: Cannon Balls, Grapeshot, and the Chainshot. The standard cannonball packs your main explosives. You can switch to the Grapeshot (which is equivalent to a shotgun) or the Chainshot (used to tackle on a ship's rigging or mast) at any given time."
      - name: 24-pounder
        link: ../galleries/weapons/#24-pounder
        image: img/game/bfp2/stationary/24pdr/main.png
        description: "The 24 lb is by far the most powerful of the cannons. This particular cannon is mainly found in fortresses and on larger ships."
  - name: Ammunition
    icon: round-shot.svg
    items:
      - name: Roundshot
        image: img/game/bfp2/weapons/ammo/roundshot.png
      - name: Grapeshot
        image: img/game/bfp2/weapons/ammo/grapeshot.png
      - name: Chainshot
        image: img/game/bfp2/weapons/ammo/chainshot.png
      - name: Coconut Bombs
        image: img/game/bfp2/weapons/ammo/coconuts.png
---
