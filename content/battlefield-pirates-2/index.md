---
title: Pirates 2
date: 2008-12-19
description: >-
  Battlefield Pirates 2 is a full conversion mod for Battlefield 2. The game
  pits 2 two rivaling crews, the Undead and the Peglegs, in a vicious battle for
  control of the Caribbean and all its booty. Brace yourself for a fun-filled
  adventure on the high seas!
categories:
  - Pirates 2
type: mod
layout: layouts/mod.njk
origin: https://web.archive.org/web/20231217145911/http://www.bfeditor.org/forums/index.php?/topic/12660-battlefield-pirates-release-v20-beta/
banner: true
sections:
  - name: Maps
    icon: compass.svg
    link: "./maps"
  - name: Classes
    icon: pirate.svg
    link: "./classes"
  - name: Weapons
    icon: sabers.svg
    link: "./weapons"
  - name: Vehicles
    icon: ship.svg
    link: "./vehicles"
  - name: Galleries
    icon: porthole.svg
    link: "./galleries"
  - name: About
    icon: scroll.svg
    link: "./about"
  - name: Audio
    icon: lighthouse.svg
    link: "./audio"
---
