---
title: About Pirates 2
categories:
  - Battlefield 2
  - Pirates 2
type: mod
layout: layouts/default.njk
origin: https://web.archive.org/web/20120510065058/http://www.bfpirates.com/wiki/index.php?title=BFP2
---

## Game Modes

A gamemode is the logic running in the background of the game, which determines how the rounds start and end, how they are scored and control the behavior of flags and game events. The logic is written in the Python language and interpreted by BF2 at run-time. The following are implemented and potential game modes, per [a post on BFPirates.com](https://web.archive.org/web/20130123205427/http://bfpirates.com/phpBB2/viewtopic.php?t=2023) from 2006.

### Conquest

- Conquest rounds are the same as the default BF2 style of play
- All R1 maps were Conquest rounds
- Teams are issued a set number of tickets at the round start
- Player deaths come off the ticket count
- If a certain number of flags are taken by one team, the other team may start losing tickets based on a timer until the balance is restored
- The more flags you lose, the faster your team's tickets are drained
- Nearly all Conquest maps come in 16, 32 and 64 player sizes - the larger the #, the larger the combat area and number of flags to fight over

### Zombie

- Zombie maps are not team based, as everyone starts on the Pegleg team and eventually winds up on the Zombie team
- The goal is to survive as a Pegleg for as long as possible
- All players start as Peglegs, except for one randomly selected player who is the first Zombie
- Anyone killed by the Zombie turns into a Zombie themselves
- Play continues until the last Pegleg is killed
- Players earn points for killing an enemy based on the number of enemies available
- The Pegleg players will not be able to hide successfully because their positions are randomly updated on the Zombie minimap/HUD every 7 seconds
- Zombies have a new melee weapon, the boarding axe, which is a one-hit kill... so dont get too close!
- We are working on a zombie player model that looks like a decaying Pegleg... who will be much harder to spot in a large crowd of enemies.

### Capture The Flag

- Each team will have only one takeable flag that can be carried on your back
- Enemy flags can be taken by touching their flagpole
- Flag carriers are free to use their weapons and cutlasses
- Flag carriers can be tracked on the minimap / HUD
- You will carry a flag until you are killed or you return to your own pole for the score
- If killed when carrying the flag, it will be dropped on the ground until recovered or re-stolen
- Players earn points for stealing a flag, returning a flag, recovering their own stolen flags as well as assistance pts for helping other players do the same
- Players cannot return a flag for a score if their own flag has been stolen
- First team to cap a set # of flags will win the round

### Armada

- Best described as a form of Naval Deathmatch
- Teams will start in uncappable ports with large fleets of ships
- Maps will be mostly open water
- Sharpshooter kits have been removed from all Armada maps
- The map will have a single neutral flag centrally positioned with a large capping radius
- The team that has superior numbers of crewmen in the radius will hoist the flag in their team's favor
- The larger the difference in crew, the faster the flag hoists
- If the flag is capped (hoisted 100%), the losing team will be penalized a large chunk of their tickets
- Losing crewmen and ships in battle also drains tickets
- All crewmen in the radius win flag capping points, not just capping assists
- All seated crewmen count for being in the radius (unlike BF2)
- Crewmen will get extra pts for manning cannons and repairing the ships
- When capped, the middle flag randomly relocates to another area and becomes neutral again - shifting the battle elsewhere
- The first team to run the enemy tickets to zero wins the round

### Rally Racing

- This gamemode was donated to us by the team at Battlefield Racer Smile
- Teams consist of Racers and Spectators
- Spectators are free to spawn in and cause chaos by shooting cannons at the Racers
- Racers can choose to race either boats or sea creatures
- Boats are better armored and have weaponry such as front mounted cannons
- Sea creatures (such as the dolphin above) have better speed, sprinting ability and can duck under or jump over obstacles
- Racers must assemble at the starting line and wait for a countdown timer (or face DQ)
- When the race begins, players must get thru a set number of checkpoints in sequence for the lap to count
- Split and lap times will be posted to the HUD
- The first Racer to successfully complete a set number of laps wins the Gold

## History

### Release 1.0

Released at [2007-09-20](https://web.archive.org/web/20240722170914/https://www.gamefront.com/games/battlefield-2/file/battlefield-pirates-2-v1-0-client-files), it included the maps Black Beards Atol, Blue Bayou, Dead Calm, Pelican Point, Shallow Draft, Shiver Me Timbers, and Storm The Bastion.

### Patch 1.1

Released at shortly after 1.0, it patches several parts of the mod.

### Patch 1.2

Released at [2007-10-06](https://web.archive.org/web/20231219164332/https://www.gamefront.com/games/battlefield-2/file/battlefield-pirates-2-v1-2-client-files), it patches several parts of the mod, and prepares the Zombie Game Mode -- though no map yet includes it.

### Beta 2.0

Released at [2008-12-18](https://web.archive.org/web/20081226082831/http://www.bfpirates.com/phpBB2/viewtopic.php?t=2096), this was a beta test [ahead of 2.1](https://web.archive.org/web/20090126222601/http://www.moddb.com/downloads/open-beta-release-2-december-2008). It added the maps Crossbones Keep, Frylar, Lost At Sea, O' Me Hearty Beach, Pressgang Port, Sailor's Warning, Shipwreck Shoals, Stranded, and Wake Island 1707.

### Release 2.1

Released at [2009-01-12](https://web.archive.org/web/20240722175042/https://www.bf-games.net/downloads/949/battlefield-pirates-bfp2-yarr21-client-full.html), [patches many parts of the mod](https://web.archive.org/web/20090122042552/http://www.bfpirates.com/phpBB2/viewtopic.php?t=2126), as well as adds Zombie and Capture The Flag Game Modes to many maps, as well as singleplayer-support and sizes to some. It is considered the proper release for the maps in 2.0.

### Release 2.5

Released at [2024-07-04](https://web.archive.org/web/20240722181546/https://www.moddb.com/mods/battlefield-pirates-2/downloads/pirates-2-v25-beta1-client-files) as a beta, it patches several parts of the mod, and added the maps Sleepy Valley, Plunders Island, Harbor, Aces High, Strike At Pegville, and Dead Mans Reef. The beta had seemingly been completed at 2010-12-17, but was not released until the map-modder Catbox shared it.

Subsequent efforts to patch known-faults was shared by Butcher in [GitLab.com/BCButcher/BattlefieldPirates2ModSource](https://gitlab.com/BCButcher/BattlefieldPirates2ModSource).

## Team

Chris "Detrux" T posted a list of staff associated with the development of Battlefield Pirates 2 in 2006 on [BFEditor.com](https://web.archive.org/web/20231217143859/http://www.bfeditor.org/forums/index.php?/topic/7-yaar-and-avast/#comment-109), as follows.

### Development

- Guy Smiley: Lead Producer
- Detrux: Project Manager

### Modeling (vehicles)

- Bunnysnot: Head

### Modeling (weapons)

- Sean “Anonymousity”
- Steven “Saw” Woods
- Brandon "Guy le Douche" Jones

### Modeling (structures)

- Sean "Ratkin" Whales
- Jimmy the Pirate

### Gamewizard

- HSD-
- Mr. P
- Mr. Corphesy

### Character Design

- Dylan ‘Macgregor “Anthony
- Samuel “Somhairle” Blake
- Ben Meals

### Textures

### SkinS

### Animation

- Dan Moreno?
- Jason ‘Animonster” Lindsay
- Erik Svensson

### Art

- Mike "Wheeljack18" Perry (Head)
- Ian James "Porthos" Collis

### Sound

- Daniel "CaptainRED" Schimanski- (Head)

### Coding

- Ade “Vex”- Coding -
- Kevin “Kev4000” Sloan-
