---
title: Pirates 2 Galleries
categories:
  - Battlefield 2
  - Pirates 2
type: media
layout: layouts/mod.njk
sections:
  - name: Concept Art
    icon: chest.svg
    link: "./concept-art"
  - name: Gameplay
    icon: paddles.svg
    link: "./gameplay"
  - name: Footage
    icon: pirate-flag.svg
    link: "./footage"
  - name: Weapons
    icon: sabers.svg
    link: "./weapons"
  - name: Vehicles
    icon: ship.svg
    link: "./vehicles"
---
