---
title: Pirates 2 Concept Art Gallery
categories:
  - Battlefield 2
  - Pirates 2
type: gallery
layout: layouts/gallery.njk
sections:
  - name: Wallpapers
    items:
      - image:
          - img/game/bfp2/concept-art/wallpapers/pirate-peninsula.jpg
          - img/game/bfp2/concept-art/wallpapers/fortress.jpg
          - img/game/bfp2/concept-art/wallpapers/sea-cliffs.jpg
          - img/game/bfp2/concept-art/wallpapers/pirates_pass.jpg
          - img/game/bfp2/concept-art/wallpapers/sea-fortress1.jpg
          - img/game/bfp2/concept-art/wallpapers/sea-fortress2.jpg
          - img/game/bfp2/concept-art/wallpapers/skeleton-island.jpg
          - img/game/bfp2/concept-art/wallpapers/volcano.jpg
          - img/game/bfp2/concept-art/wallpapers/waterfall.jpg
  - name: Buildings
    items:
      - image:
          - img/game/bfp2/concept-art/buildings/bastion-fort.jpg
          - img/game/bfp2/concept-art/buildings/building1.jpg
          - img/game/bfp2/concept-art/buildings/building2.jpg
          - img/game/bfp2/concept-art/buildings/building3.jpg
          - img/game/bfp2/concept-art/buildings/buildings1.jpg
          - img/game/bfp2/concept-art/buildings/buildings2.jpg
          - img/game/bfp2/concept-art/buildings/buildings3.jpg
          - img/game/bfp2/concept-art/buildings/docks1.jpg
          - img/game/bfp2/concept-art/buildings/docks2.jpg
          - img/game/bfp2/concept-art/buildings/powder-storage.jpg
          - img/game/bfp2/concept-art/buildings/tower.jpg
          - img/game/bfp2/concept-art/buildings/props.jpg
  - name: Kits
    items:
      - image:
          - img/game/bfp2/concept-art/kits/undead_assault.jpg
          - img/game/bfp2/concept-art/kits/pegleg-assault.jpg
          - img/game/bfp2/concept-art/kits/surgeon.jpg
          - img/game/bfp2/concept-art/kits/scout.jpg
          - img/game/bfp2/concept-art/kits/sharpshooter.jpg
          - img/game/bfp2/concept-art/kits/carpenter.jpg
          - img/game/bfp2/concept-art/kits/special-operator.jpg
          - img/game/bfp2/concept-art/kits/pistoleer.jpg
          - img/game/bfp2/concept-art/kits/assault.jpg
          - img/game/bfp2/concept-art/kits/captain.jpg
          - img/game/bfp2/concept-art/kits/sailor1.jpg
          - img/game/bfp2/concept-art/kits/sailor2.jpg
          - img/game/bfp2/concept-art/kits/sailor3.jpg
          - img/game/bfp2/concept-art/kits/skull-with-light.jpg
          - img/game/bfp2/concept-art/kits/skull1.jpg
          - img/game/bfp2/concept-art/kits/skull2.jpg
          - img/game/bfp2/concept-art/kits/skull3.jpg
          - img/game/bfp2/concept-art/kits/skull4.jpg
          - img/game/bfp2/concept-art/kits/skull5.jpg
          - img/game/bfp2/concept-art/kits/skull6.jpg
          - img/game/bfp2/concept-art/kits/skullclan1.jpg
          - img/game/bfp2/concept-art/kits/skullclan2.jpg
          - img/game/bfp2/concept-art/kits/skullclan3.jpg
          - img/game/bfp2/concept-art/kits/skullclan4.jpg
          - img/game/bfp2/concept-art/kits/pegleg.jpg
          - img/game/bfp2/concept-art/kits/abductor1.jpg
          - img/game/bfp2/concept-art/kits/abductor2.jpg
  - name: Vehicles
    items:
      - image:
          - img/game/bfp2/concept-art/vehicles/vehicles1.jpg
          - img/game/bfp2/concept-art/vehicles/shark.jpg
          - img/game/bfp2/concept-art/vehicles/komodo-dragon1.jpg
          - img/game/bfp2/concept-art/vehicles/komodo-dragon2.jpg
  - name: Weapons
    items:
      - image:
          - img/game/bfp2/concept-art/weapons/crab1.jpg
          - img/game/bfp2/concept-art/weapons/crab2.jpg
---
