---
title: Pirates 2 Gameplay
categories:
  - Battlefield 2
  - Pirates 2
type: gallery
layout: layouts/gallery.njk
sections:
  - name: Maps
    items:
      - name: Pelican Point
        image:
          - img/game/bfp2/gameplay/pelican-point/action1.jpg
          - img/game/bfp2/gameplay/pelican-point/action2.jpg
          - img/game/bfp2/gameplay/pelican-point/lighthouse1.jpg
          - img/game/bfp2/gameplay/pelican-point/lighthouse2.jpg
          - img/game/bfp2/gameplay/pelican-point/lighthouse3.jpg
          - img/game/bfp2/gameplay/pelican-point/flag.jpg
          - img/game/bfp2/gameplay/pelican-point/gun_deck.jpg
          - img/game/bfp2/gameplay/pelican-point/hallway.jpg
          - img/game/bfp2/gameplay/pelican-point/observation_deck.jpg
          - img/game/bfp2/gameplay/pelican-point/overview1.jpg
          - img/game/bfp2/gameplay/pelican-point/overview2.jpg
          - img/game/bfp2/gameplay/pelican-point/overview3.jpg
          - img/game/bfp2/gameplay/pelican-point/overview4.jpg
          - img/game/bfp2/gameplay/pelican-point/overview5.jpg
      - name: Dead Calm
        image:
          - img/game/bfp2/gameplay/dead-calm/action1.png
          - img/game/bfp2/gameplay/dead-calm/action2.png
          - img/game/bfp2/gameplay/dead-calm/action3.png
          - img/game/bfp2/gameplay/dead-calm/map-64.png
      - name: Storm The Bastion
        image:
          - img/game/bfp2/gameplay/storm-the-bastion/action01.png
          - img/game/bfp2/gameplay/storm-the-bastion/action02.jpg
          - img/game/bfp2/gameplay/storm-the-bastion/action03.jpg
          - img/game/bfp2/gameplay/storm-the-bastion/action04.jpg
          - img/game/bfp2/gameplay/storm-the-bastion/action05.jpg
          - img/game/bfp2/gameplay/storm-the-bastion/action06.jpg
          - img/game/bfp2/gameplay/storm-the-bastion/action07.jpg
          - img/game/bfp2/gameplay/storm-the-bastion/action08.jpg
          - img/game/bfp2/gameplay/storm-the-bastion/action09.jpg
          - img/game/bfp2/gameplay/storm-the-bastion/action10.jpg
          - img/game/bfp2/gameplay/storm-the-bastion/action11.jpg
          - img/game/bfp2/gameplay/storm-the-bastion/action12.jpg
          - img/game/bfp2/gameplay/storm-the-bastion/map-64.png
      - name: Shiver Me Timbers
        publisher: Unknown, Winbean winlite
        published: "Unknown, 2023-11-01"
        image:
          - img/game/bfp2/gameplay/shiver-me-timbers/action1.png
          - img/game/bfp2/gameplay/shiver-me-timbers/action2.png
          - img/game/bfp2/gameplay/shiver-me-timbers/action3.png
          - img/game/bfp2/gameplay/shiver-me-timbers/map-64.png
          - img/game/bfp2/gameplay/shiver-me-timbers/Screenshot_2023-11-01_095756.png
          - img/game/bfp2/gameplay/shiver-me-timbers/Screenshot_2023-11-01_100428.png
          - img/game/bfp2/gameplay/shiver-me-timbers/Screenshot_2023-11-01_101031.png
      - name: Shipwrech Shoals
        publisher: Winbean winlite
        published: "2023-11-01"
        image:
          - img/game/bfp2/gameplay/shipwreck-shoals/Screenshot_2023-11-01_093100.png
          - img/game/bfp2/gameplay/shipwreck-shoals/Screenshot_2023-11-01_093235.png
          - img/game/bfp2/gameplay/shipwreck-shoals/Screenshot_2023-11-01_093535.png
          - img/game/bfp2/gameplay/shipwreck-shoals/Screenshot_2023-11-01_093632.png
          - img/game/bfp2/gameplay/shipwreck-shoals/Screenshot_2023-11-01_093716.png
      - name: Harbor
        publisher: Frosty
        published: "2024-11-03"
        image:
          - img/game/bfp2/gameplay/harbor/rat.png
      - name: Blue Bayou
        image:
          - img/game/bfp2/gameplay/bluebayou/action1.jpg
          - img/game/bfp2/gameplay/bluebayou/action2.jpg
          - img/game/bfp2/gameplay/bluebayou/action3.png
          - img/game/bfp2/gameplay/bluebayou/action4.png
          - img/game/bfp2/gameplay/bluebayou/action5.png
          - img/game/bfp2/gameplay/bluebayou/map-64.png
      - name: Shallow Draft
        publisher: Unknown, Winbean winlite
        published: "Unknown, 2023-11-01"
        image:
          - img/game/bfp2/gameplay/shallow-draft/action1.png
          - img/game/bfp2/gameplay/shallow-draft/action2.png
          - img/game/bfp2/gameplay/shallow-draft/action3.png
          - img/game/bfp2/gameplay/shallow-draft/Screenshot_2023-11-01_075934.png
          - img/game/bfp2/gameplay/shallow-draft/Screenshot_2023-11-01_080011.png
          - img/game/bfp2/gameplay/shallow-draft/Screenshot_2023-11-01_080106.png
          - img/game/bfp2/gameplay/shallow-draft/Screenshot_2023-11-01_080134.png
          - img/game/bfp2/gameplay/shallow-draft/Screenshot_2023-11-01_080144.png
      - name: Black Beards Atol
        image:
          - img/game/bfp2/gameplay/black-beards-atol/action1.png
          - img/game/bfp2/gameplay/black-beards-atol/action2.png
          - img/game/bfp2/gameplay/black-beards-atol/action3.png
          - img/game/bfp2/gameplay/black-beards-atol/map-64.png
      - name: Pressgang Port
        publisher: Frosty
        published: "2024-11-03"
        image:
          - img/game/bfp2/gameplay/pressgang-port/drying-shark.png
      - name: Sailors Warning
        publisher: Unknown, Winbean winlite
        published: "Unknown, 2023-11-01"
        image:
          - img/game/bfp2/gameplay/sailors-warning/action1.jpg
          - img/game/bfp2/gameplay/sailors-warning/action2.jpg
          - img/game/bfp2/gameplay/sailors-warning/action3.jpg
          - img/game/bfp2/gameplay/sailors-warning/action4.jpg
          - img/game/bfp2/gameplay/sailors-warning/Screenshot_2023-11-01_103204.png
          - img/game/bfp2/gameplay/sailors-warning/Screenshot_2023-11-01_103302.png
      - name: Dead Mans Reef
        publisher: Unknown, Frosty
        published: "Unknown, 2024-11-03"
        image:
          - img/game/bfp2/gameplay/dead-mans-reef/coral-reef1.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/coral-reef2.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/coral-reef3.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/dolphin.png
          - img/game/bfp2/gameplay/dead-mans-reef/galleon1.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/galleon10.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/galleon11.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/galleon12.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/galleon13.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/galleon14.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/galleon15.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/galleon16.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/galleon17.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/galleon18.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/galleon19.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/galleon2.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/galleon20.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/galleon21.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/galleon22.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/galleon23.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/galleon24.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/galleon25.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/galleon26.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/galleon27.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/galleon28.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/galleon29.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/galleon3.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/galleon30.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/galleon4.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/galleon5.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/galleon6.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/galleon7.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/galleon8.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/galleon9.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/islands1.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/islands2.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/islands3.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/islands4.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/islands5.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/islands6.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/map-16.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/map-32.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/map-64-ctf.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/map-64.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/map-design.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/map.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/pegleg-bay1.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/pegleg-bay10.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/pegleg-bay11.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/pegleg-bay12.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/pegleg-bay2.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/pegleg-bay3.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/pegleg-bay4.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/pegleg-bay5.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/pegleg-bay6.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/pegleg-bay7.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/pegleg-bay8.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/pegleg-bay9.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/sinking1.png
          - img/game/bfp2/gameplay/dead-mans-reef/sinking2.png
          - img/game/bfp2/gameplay/dead-mans-reef/sinking3.png
          - img/game/bfp2/gameplay/dead-mans-reef/sinking4.png
          - img/game/bfp2/gameplay/dead-mans-reef/sinking5.png
          - img/game/bfp2/gameplay/dead-mans-reef/undead-bay1.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/undead-bay10.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/undead-bay11.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/undead-bay12.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/undead-bay2.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/undead-bay3.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/undead-bay4.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/undead-bay5.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/undead-bay6.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/undead-bay7.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/undead-bay8.jpg
          - img/game/bfp2/gameplay/dead-mans-reef/undead-bay9.jpg
  - name: Other
    items:
      - image:
          - img/game/bfp2/gameplay/gnat.jpg
          - img/game/bfp2/gameplay/grog.jpg
          - img/game/bfp2/gameplay/parrot.jpg
          - img/game/bfp2/gameplay/parrot_fill.jpg
          - img/game/bfp2/gameplay/powder_barrel.jpg
          - img/game/bfp2/gameplay/powder_barrel_boom.jpg
          - img/game/bfp2/gameplay/wreck.jpg
          - img/game/bfp2/gameplay/bensta-grave.png
          - img/game/bfp2/gameplay/jimmy-grave.png
---
