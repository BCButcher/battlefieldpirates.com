---
title: Pirates 2 Weapons Gallery
categories:
  - Battlefield 2
  - Pirates 2
type: gallery
layout: layouts/gallery.njk
sections:
  - name: Handheld
    items:
      - name: Cutlass
        image: img/game/bfp2/weapons/handheld/cutlass/render.jpg
      - name: Pistol
        image: img/game/bfp2/weapons/handheld/pistol/art.jpg
      - name: Pistolette / French Pistol
        image: img/game/bfp2/weapons/handheld/pistolette/art.jpg
      - name: Ducksfoot Pistol
        image: img/game/bfp2/weapons/handheld/ducksfoot/art.jpg
      - name: Blunderbuss
        image:
          - img/game/bfp2/weapons/handheld/blunderbuss/art.jpg
          - img/game/bfp2/weapons/handheld/blunderbuss/render1.png
          - img/game/bfp2/weapons/handheld/blunderbuss/render2.jpg
      - name: Musketoon
        image: img/game/bfp2/weapons/handheld/musketoon/art.jpg
      - name: Musket
        image: img/game/bfp2/weapons/handheld/musket/render.jpg
      - name: Sharpshooter’s Musket
        image: img/game/bfp2/weapons/handheld/sharpshooters_musket/art.jpg
      - name: Grenade
        image: img/game/bfp2/weapons/utility/grenade/art.jpg
      - name: Grenade Launcher
        image: img/game/bfp2/weapons/handheld/grenade_launcher/art.jpg
  - name: Utilities
    items:
      - name: Smoke Grenade
        image: img/game/bfp2/weapons/utility/smoke_jar/art.jpg
      # - name: Flaming Potionjar
      - name: Powder Keg
        image:
          - img/game/bfp2/weapons/utility/powder_keg/art.jpg
          - img/game/bfp2/weapons/utility/powder_keg/render.png
      - name: Crab
        image: img/game/bfp2/weapons/utility/crab/art.jpg
      # - name: Parrot
      # - name: Flare Launcher
      # - name: Grappling Hook
      # - name: Zipline Crossbow
      - name: Hammer
        image: img/game/bfp2/weapons/utility/hammer/render.jpg
      - name: Spyglass
        image: img/game/bfp2/weapons/utility/spyglass/art.jpg
      # - name: Ammunition Bag
      - name: Surgeon Toolkit and Grog Keg
        image: img/game/bfp2/weapons/utility/surgeon_toolkit/render.jpg
  - name: Cannons
    items:
      # - name: Field Cannon
      - name: Falconet
        image:
          - img/game/bfp2/stationary/falconet/art.jpg
          - img/game/bfp2/stationary/falconet/art2.jpg
          - img/game/bfp2/stationary/falconet/render.png
      - name: 12-pounder
        image: img/game/bfp2/stationary/12pdr/art.jpg
      - name: 24-pounder
        image: img/game/bfp2/stationary/24pdr/art.jpg
  # - name: Ammunition
  #   items:
  #     - name: Roundshot
  #     - name: Grapeshot
  #     - name: Chainshot
  #     - name: Coconut Bombs
---
