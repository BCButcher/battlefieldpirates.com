---
title: Halloween Event 2020
description: "2020-10-31"
categories:
  - Battlefield 2
  - Pirates 2
origin: https://web.archive.org/web/20241006072235/https://www.lost-soldiers.org/v2.php?site=forum_topic&topic=1634
image: img/community/bfp2/events/2020-10-31.png
type: exhibit
layout: layouts/exhibit.njk
items:
  - name: "Battlefield 2- Pirates 2 mod,Halloween event #2"
    image: img/game/bfp2/video/youtube-oCF6JifE1LQ.jpg
    link: "https://www.youtube.com/watch?v=oCF6JifE1LQ"
    publisher: Spagati Yeti
    published: "2020-11-16"
  - name: Battlefield 2 Online  - Discount Sea of Thieves (Lost Soldiers Halloween Event)
    image: img/game/bfp2/video/youtube-sZe2CP5DMPc.jpg
    link: "https://www.youtube.com/watch?v=sZe2CP5DMPc"
    publisher: VikingBnS
    published: "2020-11-10"
  - name: "BF2 Pirates Mod in 2020: Halloween Event by Lost Soldiers Community"
    image: img/game/bfp2/video/youtube-fkp0iIscObU.jpg
    link: "https://www.youtube.com/watch?v=fkp0iIscObU"
    publisher: GravityBFTV
    published: "2020-11-05"
  - name: Halloween  Battlefield 2  pirates 2  event  part 1
    image: img/game/bfp2/video/youtube--iwGSh57Uxc.webp
    link: "https://www.youtube.com/watch?v=-iwGSh57Uxc"
    publisher: E3gaming
    published: "2020-11-03"
  - name: Halloween  Battlefield 2  pirates 2  event  part 2
    image: img/game/bfp2/video/youtube-qrJNh0mKyCI.webp
    link: "https://www.youtube.com/watch?v=qrJNh0mKyCI"
    publisher: E3gaming
    published: "2020-11-01"
  - name: Halloween  Battlefield 2  pirates 2  event  part 3
    image: img/game/bfp2/video/youtube-bfQHDm09F9M.webp
    link: "https://www.youtube.com/watch?v=bfQHDm09F9M"
    publisher: E3gaming
    published: "2020-11-01"
  - name: Halloween  Battlefield 2  pirates 2  event  part 5
    image: img/game/bfp2/video/youtube-oNRJFHg5IhY.webp
    link: "https://www.youtube.com/watch?v=oNRJFHg5IhY"
    publisher: E3gaming
    published: "2020-11-01"
---
