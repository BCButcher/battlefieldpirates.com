---
title: Halloween Event 2023
description: "2023-10-29"
categories:
  - Battlefield 2
  - Pirates 2
origin: https://web.archive.org/save/https://www.lost-soldiers.org/v2.php?site=forum_topic&topic=2223
image: img/community/bfp2/events/2023-10-29.jpg
type: exhibit
layout: layouts/exhibit.njk
items:
  - name: "Skeletons vs. Pirates: Haunting the High Seas - Halloween Event on Shallow Draft"
    image: img/game/bfp2/video/youtube-9HRoxAh8TYY.jpg
    link: "https://www.youtube.com/watch?v=9HRoxAh8TYY"
    publisher: Winbean winlite
    published: "2023-11-11"
  - name: "Shiver Me Timbers: High Seas Adventures in Battlefield Pirates 2"
    image: img/game/bfp2/video/youtube-QuWcneY7-Ds.jpg
    link: "https://www.youtube.com/watch?v=QuWcneY7-Ds"
    publisher: Winbean winlite
    published: "2023-11-11"
  - name: "Storm the Bastion: Zombie Mayhem in Battlefield Pirates 2"
    image: img/game/bfp2/video/youtube-4ADGxarBj-w.jpg
    link: "https://www.youtube.com/watch?v=4ADGxarBj-w"
    publisher: Winbean winlite
    published: "2023-11-11"
  - name: "Daylight Duels: Battling at Shipwreck Shoals in Battlefield Pirates"
    image: img/game/bfp2/video/youtube-y8o_iJ4AeSE.jpg
    link: "https://www.youtube.com/watch?v=y8o_iJ4AeSE"
    publisher: Winbean winlite
    published: "2023-11-11"
  - name: "Frylar Zombie Hunt: Nighttime Terror in Battlefield Pirates 2"
    image: img/game/bfp2/video/youtube-tuLlHlewJ68.jpg
    link: "https://www.youtube.com/watch?v=tuLlHlewJ68"
    publisher: Winbean winlite
    published: "2023-11-11"
  - name: "Reddish Fog Ambush: 'Sailor's Warning' Naval Battle in Battlefield Pirates 2"
    image: img/game/bfp2/video/youtube-h5b-ZCrF7gU.webp
    link: "https://www.youtube.com/watch?v=h5b-ZCrF7gU"
    publisher: Winbean winlite
    published: "2023-10-31"
  - name: "Battlefield Pirates 2 / Йо-хо-хо и Battlefield 2 на Хэллоуин c Lost-Soldiers /// Трансляция"
    image: img/game/bfp2/video/youtube-tOea-5boKII.webp
    link: "https://www.youtube.com/watch?v=tOea-5boKII"
    publisher: LastofAvari Belarus
    published: "2023-10-29"
---
