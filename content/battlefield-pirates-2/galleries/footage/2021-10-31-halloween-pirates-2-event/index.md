---
title: Halloween Event 2021
description: "2021-10-31"
categories:
  - Battlefield 2
  - Pirates 2
origin: https://web.archive.org/web/20241006071739/https://www.lost-soldiers.org/v2.php?site=forum_topic&topic=1930
image: img/community/bfp2/events/2021-10-31.png
type: exhibit
layout: layouts/exhibit.njk
items:
  - name: BF2 Halloween Event 2021 ｜ Pirates 2 Mod
    image: img/game/bfp2/video/youtube-XSHWDSN7Qlc.jpg
    link: "https://www.youtube.com/watch?v=XSHWDSN7Qlc"
    publisher: Tyler Black BF2
    published: "2021-11-14"
  - name: Battlefield Pirates 2  Halloween event 2021 part 1
    image: img/game/bfp2/video/youtube-Q-6wMvEscEQ.webp
    link: "https://www.youtube.com/watch?v=Q-6wMvEscEQ"
    publisher: E3gaming
    published: "2021-11-03"
  - name: Battlefield Pirates 2  Halloween event 2021 part 2
    image: img/game/bfp2/video/youtube-5ZnxUxcesD0.webp
    link: "https://www.youtube.com/watch?v=5ZnxUxcesD0"
    publisher: E3gaming
    published: "2021-11-02"
  - name: Battlefield Pirates 2  Halloween event 2021 part 3
    image: img/game/bfp2/video/youtube-AZ3P7VHlxdM.webp
    link: "https://www.youtube.com/watch?v=AZ3P7VHlxdM"
    publisher: E3gaming
    published: "2021-11-02"
  - name: Battlefield Pirates 2  Halloween event 2021 part 4
    image: img/game/bfp2/video/youtube-DXOVPNiwRNE.webp
    link: "https://www.youtube.com/watch?v=DXOVPNiwRNE"
    publisher: E3gaming
    published: "2021-11-01"
  - name: Battlefield Pirates 2  Halloween event 2021 part 5
    image: img/game/bfp2/video/youtube-bjWlP6kkCqw.webp
    link: "https://www.youtube.com/watch?v=bjWlP6kkCqw"
    publisher: E3gaming
    published: "2021-11-01"
  - name: "Battlefield: Pirates 2 - Lost-Soldiers Multiplayer Event ｜ February 6th, 2021"
    image: img/game/bfp2/video/youtube-bkeQDwWQhy4.jpg
    link: "https://www.youtube.com/watch?v=bkeQDwWQhy4"
    publisher: Prospekt Mir
    published: "2021-02-11"
  - name: Battlefield 2  pirates 2  event from 2021 feb
    image: img/game/bfp2/video/youtube-PintrLuKo4o.webp
    link: "https://www.youtube.com/watch?v=PintrLuKo4o"
    publisher: E3gaming
    published: "2021-02-07"
---
