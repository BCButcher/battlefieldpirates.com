---
title: Pirates 2 Footage
categories:
  - Battlefield 2
  - Pirates 2
type: gallery
layout: layouts/gallery.njk
sections:
  - name: Events
    items:
      - name: Halloween Event 2024
        link: "./2024-11-02-halloween-pirates-2-event/"
        description: "2024-11-02"
        origin: https://web.archive.org/web/20241117203209/https://www.lost-soldiers.org/v2.php?site=forum_topic&topic=2390
        image: img/community/bfp2/events/2024-11-02.jpg
      - name: Halloween Event 2023
        link: "./2023-10-29-halloween-pirates-2-event/"
        description: "2023-10-29"
        origin: https://web.archive.org/save/https://www.lost-soldiers.org/v2.php?site=forum_topic&topic=2223
        image: img/community/bfp2/events/2023-10-29.png
      - name: Halloween Event 2022
        link: "./2022-10-30-halloween-pirates-2-event/"
        description: "2023-10-30"
        origin: https://web.archive.org/web/20241006071356/https://www.lost-soldiers.org/v2.php?site=forum_topic&topic=2057
        image: img/community/bfp1/events/2022-10-28+30.png
      - name: Halloween Event 2021
        link: "./2021-10-31-halloween-pirates-2-event/"
        description: "2021-10-31"
        origin: https://web.archive.org/web/20241006071739/https://www.lost-soldiers.org/v2.php?site=forum_topic&topic=1930
        image: img/community/bfp2/events/2021.10.31.png
      - name: Halloween Event 2020
        link: "./2020-10-31-halloween-pirates-2-event/"
        description: "2020-10-31"
        origin: https://web.archive.org/web/20241006072235/https://www.lost-soldiers.org/v2.php?site=forum_topic&topic=1634
        image: img/community/bfp2/events/2020-10-31.png
  - name: Older
    items:
      - name: "Battlefield Pirates 2: The Galleon"
        image: img/game/bfp2/video/youtube-CymKlaGu2k8.jpg
        link: "https://www.youtube.com/watch?v=CymKlaGu2k8"
        publisher: AfterDune
        published: "2011-08-19"
      - name: Battlefield Pirates 2 - Capture the Flag Gamemode
        image: img/game/bfp2/video/youtube-ZIWgp3U6an0.jpg
        link: "https://www.youtube.com/watch?v=ZIWgp3U6an0"
        publisher: 1l2Hawk
        published: "2008-09-05"
      - name: Battlefield Pirates 2 - Conquest Gamemode Round
        image: img/game/bfp2/video/youtube-pkpCMewVkQU.jpg
        link: "https://www.youtube.com/watch?v=pkpCMewVkQU"
        publisher: 1l2Hawk
        published: "2008-09-05"
      - name: Battlefield Pirates 2 - Release 2 Maps
        image: img/game/bfp2/video/youtube-CCrdW7lTmz0.jpg
        link: "https://www.youtube.com/watch?v=CCrdW7lTmz0"
        publisher: 1l2Hawk
        published: "2008-07-02"
      - name: Battlefield Pirates 2 - Zombie Gamemode Round
        image: img/game/bfp2/video/youtube-FUqKGG48Q30.jpg
        link: "https://www.youtube.com/watch?v=FUqKGG48Q30"
        publisher: 1l2Hawk
        published: "2008-09-05"
      - name: Battlefield Pirates 2 - Zombie Gamemode Round 2
        image: img/game/bfp2/video/youtube-i7cwP3q26kc.jpg
        link: "https://www.youtube.com/watch?v=i7cwP3q26kc"
        publisher: 1l2Hawk
        published: "2008-09-05"
      - name: Poly Wants A (Fire)Cracker!  Things that go boom in BFP2
        image: img/game/bfp2/video/youtube-U_O4GW0P3zk.jpg
        link: "https://www.youtube.com/watch?v=U_O4GW0P3zk"
        publisher: 1l2Hawk
        published: "2008-08-11"
      - name: BFP2 - Storm the Bastion - Mapping Test 4a
        image: img/game/bfp2/video/youtube-CjdDERrQjuA.jpg
        link: "https://www.youtube.com/watch?v=CjdDERrQjuA"
        publisher: 1l2Hawk
        published: "2007-01-11"
      - name: BFP2 - Storm the Bastion - Play Test 4b
        image: img/game/bfp2/video/youtube-Kylml9Wu2b0.jpg
        link: "https://www.youtube.com/watch?v=Kylml9Wu2b0"
        publisher: 1l2Hawk
        published: "2007-01-12"
      - name: BFP2 - Storm the Bastion - Triggerable Gate
        image: img/game/bfp2/video/youtube-6H1fJ3_gYmE.jpg
        link: "https://www.youtube.com/watch?v=6H1fJ3_gYmE"
        publisher: 1l2Hawk
        published: "2007-03-27"
      - name: BFP2 - Storm the Bastion - Mapping Test 3
        image: img/game/bfp2/video/youtube-plUWWCXbwrk.jpg
        link: "https://www.youtube.com/watch?v=plUWWCXbwrk"
        publisher: 1l2Hawk
        published: "2006-12-07"
      - name: BFP2 - Storm the Bastion - Mapping Test 2
        image: img/game/bfp2/video/youtube-uDgCbUk0BJY.jpg
        link: "https://www.youtube.com/watch?v=uDgCbUk0BJY"
        publisher: 1l2Hawk
        published: "2006-11-30"
---
