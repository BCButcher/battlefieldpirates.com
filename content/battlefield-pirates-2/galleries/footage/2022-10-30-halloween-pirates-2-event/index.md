---
title: Halloween Event 2022
description: "2022-10-30"
categories:
  - Battlefield 2
  - Pirates 2
origin: https://web.archive.org/web/20241006071356/https://www.lost-soldiers.org/v2.php?site=forum_topic&topic=2057
image: img/community/bfp1/events/2022-10-28+30.png
type: exhibit
layout: layouts/exhibit.njk
items:
  - name: "Battlefield Pirates 2 /// Личное Мнение"
    image: img/game/bfp2/video/youtube-_WuAJ65t5w0.webp
    link: "https://www.youtube.com/watch?v=_WuAJ65t5w0"
    publisher: LastofAvari Belarus
    published: "2022-12-21"
  - name: 2022 Battlefield 2 Halloween Event in Pirates Mod!
    image: img/game/bfp2/video/youtube-YmlkjZ7Szlw.jpg
    link: "https://www.youtube.com/watch?v=YmlkjZ7Szlw"
    publisher: Dominiko
    published: "2022-11-03"
  - name: Battlefield 2 Pirates 2 Halloween event from  30th of October 2022 part 1
    image: img/game/bfp2/video/youtube-ygotLDvCVi0.webp
    link: "https://www.youtube.com/watch?v=ygotLDvCVi0"
    publisher: E3gaming
    published: "2022-11-01"
  - name: Battlefield 2 Pirates 2 Halloween event from  30th of October 2022 part 2
    image: img/game/bfp2/video/youtube-KGSFzP60OiI.webp
    link: "https://www.youtube.com/watch?v=KGSFzP60OiI"
    publisher: E3gaming
    published: "2022-11-01"
  - name: Battlefield 2, October 30, 2022 pirate event
    image: img/game/bfp2/video/rumble-v1quy94.jpg
    link: "https://rumble.com/v1quy94-battlefield-2-october-30-2022-pirate-event.html"
    publisher: Winbean45
    published: "2022-10-30"
  - name: Battlefield 2, October 30, 2022 Pirate 2 event, MAP BLACK BEARDS ATOL
    image: img/game/bfp2/video/rumble-v1qxdyi.jpg
    link: "https://rumble.com/v1qxdyi-battlefield-2-october-30-2022-pirate-2-event-map-black-beards-atol.html"
    publisher: Winbean45
    published: "2022-10-30"
  - name: Battlefield 2, October 30, 2022 Pirate 2 event, MAP CROSSBONES KEEPS
    image: img/game/bfp2/video/rumble-v1qvkq6.jpg
    link: "https://rumble.com/v1qvkq6-battlefield-2-october-30-2022-pirate-2-event-map-crossbones-keeps.html"
    publisher: Winbean45
    published: "2022-10-30"
  - name: Battlefield 2, October 30, 2022 Pirate 2 event, MAP CROSSBONES KEEPS, Round 2
    image: img/game/bfp2/video/rumble-v1qvpys.jpg
    link: "https://rumble.com/v1qvpys-battlefield-2-october-30-2022-pirate-2-event-map-crossbones-keeps-round-2..html"
    publisher: Winbean45
    published: "2022-10-30"
  - name: Battlefield 2, October 30, 2022 pirate 2 event, MAP DEAD CALM
    image: img/game/bfp2/video/rumble-v1qv4no.jpg
    link: "https://rumble.com/v1qv4no-battlefield-2-october-30-2022-pirate-2-event-map-dead-calm.html"
    publisher: Winbean45
    published: "2022-10-30"
  - name: Battlefield 2, October 30, 2022 pirate 2 event, map FRYLAR - ZOMBIE
    image: img/game/bfp2/video/rumble-v1qv1tq.jpg
    link: "https://rumble.com/v1qv1tq-battlefield-2-october-30-2022-pirate-2-event-map-frylar-zombie..html"
    publisher: Winbean45
    published: "2022-10-30"
  - name: Battlefield 2, October 30, 2022 pirate 2 event, map FRYLAR - ZOMBIE, Round 2
    image: img/game/bfp2/video/rumble-v1qv31w.jpg
    link: "https://rumble.com/v1qv31w-battlefield-2-october-30-2022-pirate-2-event-map-frylar-zombie-round-2.html"
    publisher: Winbean45
    published: "2022-10-30"
---
