---
title: Halloween Event 2024
description: "2024-11-02"
categories:
  - Battlefield 2
  - Pirates 2
origin: https://web.archive.org/web/20241117203209/https://www.lost-soldiers.org/v2.php?site=forum_topic&topic=2390
image: img/community/bfp2/events/2024-11-02.jpg
type: exhibit
layout: layouts/exhibit.njk
items:
  - name: "BF2 Lost-Soldiers Pirates Event 2024 in 4K"
    image: img/game/bfp2/video/youtube-y6I0uTobEgo.webp
    link: "https://www.youtube.com/watch?v=y6I0uTobEgo"
    publisher: Spooky
    published: "2024-11-03"
---
