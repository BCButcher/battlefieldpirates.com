---
title: Pirates 2 Vehicles Gallery
categories:
  - Battlefield 2
  - Pirates 2
type: gallery
layout: layouts/gallery.njk
sections:
  - name: Ships Of The Line
    items:
      - name: Galleon / Tallship
        image: img/game/bfp2/vehicles/galleon/action.jpg
      - name: Frigate
        image:
          - img/game/bfp2/vehicles/frigate/original.jpg
          - img/game/bfp2/vehicles/frigate/action1.jpg
          - img/game/bfp2/vehicles/frigate/action2.jpg
          - img/game/bfp2/vehicles/frigate/action3.jpg
          - img/game/bfp2/vehicles/frigate/action4.jpg
      - name: Brig
        image:
          - img/game/bfp2/vehicles/brig/art.jpg
          - img/game/bfp2/vehicles/brig/original.jpg
          - img/game/bfp2/vehicles/brig/action1.jpg
          - img/game/bfp2/vehicles/brig/action2.jpg
          - img/game/bfp2/vehicles/brig/action3.jpg
          - img/game/bfp2/vehicles/brig/action3.jpg
          - img/game/bfp2/vehicles/brig/render1.jpg
          - img/game/bfp2/vehicles/brig/render2.jpg
      - name: Cutter
        image: img/game/bfp2/vehicles/cutter/action.jpg
      - name: Wherry
        image:
          - img/game/bfp2/vehicles/wherry/original.jpg
          - img/game/bfp2/vehicles/wherry/action1.jpg
          - img/game/bfp2/vehicles/wherry/action2.jpg
          - img/game/bfp2/vehicles/wherry/action3.jpg
          - img/game/bfp2/vehicles/wherry/action4.jpg
          - img/game/bfp2/vehicles/wherry/render.jpg
      - name: Runabout
        image:
          - img/game/bfp2/vehicles/runabout/art.jpg
          - img/game/bfp2/vehicles/runabout/action.jpg
  - name: Boats
    items:
      - name: Gnat
        image: img/game/bfp2/vehicles/gnat/art.jpg
      - name: Dinghy
        image: img/game/bfp2/vehicles/dinghy/art.jpg
  - name: Other
    items:
      - name: Balloon
        image:
          - img/game/bfp2/vehicles/balloon/art.jpg
          - img/game/bfp2/vehicles/balloon/action.jpg
      - name: Biergleiter
        image:
          - img/game/bfp2/vehicles/biergleiter/art.jpg
          - img/game/bfp2/vehicles/biergleiter/action.jpg
---
