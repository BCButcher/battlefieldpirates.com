---
title: Pirates 2 Vehicles
categories:
  - Battlefield 2
type: mod
layout: layouts/details.njk
image: img/game/bfp2/vehicles/overview.jpg
details:
  - name: Ships Of The Line
    icon: ship.svg
    items:
      - name: Galleon / Tallship
        link: ../galleries/vehicles/#galleon-tallship
        image: img/game/bfp2/vehicles/galleon/main.jpg
        points:
          - Only the static model exists in Pirates 2
      - name: Frigate
        link: ../galleries/vehicles/#frigate
        image:
          - img/game/bfp2/vehicles/frigate/main.png
          - img/game/bfp2/vehicles/frigate/top.png
        points:
          - 4 6lb adjustable cannons on each side
          - 4 6lb crew-cannons on each side
          - 1 Helmsman, 4 Cannoniers, 1 Bird's Nest Crewmate
        description: "The largest vessel in the pirate fleet. It holds an array of massive 12lb cannons which can lay waste to any ship caught in its path. This ship is usually rewarded to a side for capturing a key location."
      - name: Brig
        link: ../galleries/vehicles/#brig
        image: img/game/bfp2/vehicles/brig/main.png
        points:
          - 3 6lb adjustable cannons on each side
          - 2 6lb crew-cannons on each side
          - 1 rear-mounted 2lb Falconet Bowchasers
          - 1 Helmsman, 4 Cannoniers, 3 Falconiers, up to 50 Crewmates
          - 15 knots top speed
          - More than 300 tons displacement
        description: "Armed with both crew and captain controllable side cannons, and three mounted swivel falconets for defense, this ship is a deadly force.\n\nAs with every large ship, there are two locations on the sides where an enemy player can be beamed to the deck to engage the crew with cutlasses drawn. On the Brig these are located exactly midship."
      - name: Cutter
        link: ../galleries/vehicles/#cutter
        image: img/game/bfp2/vehicles/cutter/top.png
        points:
          - 4 adjustable cannons on each side
          - 2 front-mounted 2lb Falconet Bowchasers, firing grapeshot
          - 1 Helmsman, 2 Gunners, up to 25 Crewmates
          - 20 knots top speed
          - More than 220 tons displacement
        description: "The Cutter is one of the smallest fighting ships on the high seas, usually only used for scouting, and attacking lone merchant ships due to the small armorment it carried consisting of eight carronades, four mounted on each side of the hull and two swivel carronades mounted in the bow of the ship acting as bowchasers. Cutters are usually deep draughted ships and therefore carry large sails allowing the ship to be driven though the water at speed, in good winds anything up to 20 knots could be achieved thus allowing it to escape from most more heavily armored opponents.\n\nBecause of their small size and inability to hold much fresh water, cutters remained fairly close to larger ships or to ports where they could pick-up supplies, and most importantly where their pirate crew could sell their ill gotten plundered goods. Yet with most ports colonially controlled and hostile to pirates they relied upon free ports and ports abandoned by the Colonials, such as the town in the bay below the Bastion, where pirates ruled both the sea and land."
      - name: Wherry
        link: ../galleries/vehicles/#wherry
        image: img/game/bfp2/vehicles/wherry/main.png
        points:
          - 2 front-mounted 6lb adjustable cannons
          - 1 Helmsman, 2 Gunners, 2 seated Crewmates
          - Re-supplies nearby friendly ships
        description: "A support craft, ideal in situations of refueling and refreshing ship docks with fresh sailors."
      - name: Runabout
        link: ../galleries/vehicles/#runabout
        image: img/game/bfp2/vehicles/runabout/top.png
        description: "The Runabout is a small single mast craft used for transporting pirate crews at high speeds. Runabouts are armed with a single swivel falconet at the bow. Skilled drivers can use the boats light weight to skim across short land distances to travel more efficiently."
        points:
          - 1 mounted 2lb Falconet Bowchaser
          - 1 Helmsman, 4 seated Crewmates
          - Mobile spawn point for if the craft has a Helmsman
  - name: Boats
    icon: boat.svg
    items:
      - name: Gnat
        link: ../galleries/vehicles/#gnat
        image: img/game/bfp2/vehicles/gnat/top.png
        description: "The gnat is a small craft bearing stark resemblance to the dinghy, except this ship is armed with a fore-mounted 6lb cannon. The round can propel the craft backwards from the kickback though.\n\nThis ship only seats one pirate, though when you exit you will find yourself still in the boat. This makes it easier to toss a grappling hook from the boat."
        points:
          - 1 6lb adjustable cannon
      - name: Dinghy
        link: ../galleries/vehicles/#dinghy
        image: img/game/bfp2/vehicles/dinghy/top.png
        description: "The Dinghy is a small rowboat which seats two pirates, and is propelled by rowing power. It did could not travel very fast, but served as a quick ferry between islands or ships. The dinghy could also serve as a firing platform for sniping stranded sailors."
  - name: Other
    icon: parrot.svg
    items:
      - name: Balloon
        link: ../galleries/vehicles/#balloon
        image: img/game/bfp2/vehicles/balloon/action.jpg
        description: "The dastardly hot air balloon beckons pirates into the next frontiers; the sky!"
        points:
          - 4 attached Powder Kegs
          - 2 front-mounted 2lb Falconet Bowchasers
      - name: Biergleiter
        link: ../galleries/vehicles/#biergleiter
        image: img/game/bfp2/vehicles/biergleiter/action.jpg
        description: "By far the strangest contraption to be engineered by an especially drunk sailor, this beer keg has been shaken up vigerously and given sails and a cannon as to fly around and bombard other pirates."
        points:
          - 1 front-mounted Falconet, firing grapeshot
          - 5 rounds of attached Coconut Bombs
      - name: Dolphin
        image: img/game/bfp2/vehicles/dolphin/action.jpg
        points:
          - Can dive underwater
---
