---
title: Pirates 2 Audio
categories:
  - Battlefield 2
  - Pirates 2
type: audio
layout: layouts/audio.njk
sections:
  - name: Soundtrack
    description: "Original music for Battlefield Pirates 2 for PC, released [June 24 2006 by Matt Pyter](https://mattpyter.bandcamp.com/album/battlefield-pirates-2).\n\n'Compositions are entirely my own - except for 10 (sample of a Miles Davis trumpet part) and 12 (this map had a team raffle to win and choose any song for me to \"cover\" - the winner chose for me to orchestrate \"Red Sky\" by band Thrice). Please note that I did not name any of the maps.'\n\nLicense: CC BY-NC-ND 3.0 DEED - Attribution-NonCommercial-NoDerivs 3.0 Unported"
    origin: https://web.archive.org/web/20080901123344/http://www.bfpirates.com/?Page=Downloads
    items:
      - name: Overture
        url: "/audio/bfp2/soundtrack/01-Overture.mp3"
      - name: Pirate Waltz
        url: "/audio/bfp2/soundtrack/02-Pirate-Waltz.mp3"
      - name: Pelican Point
        url: "/audio/bfp2/soundtrack/03-Pelican-Point.mp3"
      - name: Dead Calm
        url: "/audio/bfp2/soundtrack/04-Dead-Calm.mp3"
      - name: Shiver Me Timbers
        url: "/audio/bfp2/soundtrack/05-Shiver-Me-Timbers.mp3"
      - name: Storm the Bastion
        url: "/audio/bfp2/soundtrack/06-Storm-the-Bastion.mp3"
      - name: Black Beards Atol
        url: "/audio/bfp2/soundtrack/07-Black-Beards-Atol.mp3"
      - name: Lost At Sea
        url: "/audio/bfp2/soundtrack/08-Lost-At-Sea.mp3"
      - name: Shallow Draft
        url: "/audio/bfp2/soundtrack/09-Shallow-Draft.mp3"
      - name: Blue Bayou
        url: "/audio/bfp2/soundtrack/10-Blue-Bayou.mp3"
      - name: Wake 1707
        url: "/audio/bfp2/soundtrack/11-Wake-1707.mp3"
      - name: Sailors Warning
        url: "/audio/bfp2/soundtrack/12-Sailors-Warning.mp3"
      - name: Oh Me Hearties Beach
        url: "/audio/bfp2/soundtrack/13-Oh-Me-Hearties-Beach.mp3"
      - name: Crossbones Keep
        url: "/audio/bfp2/soundtrack/14-Crossbones-Keep.mp3"
      - name: Pressgang Port
        url: "/audio/bfp2/soundtrack/15-Pressgang-Port.mp3"
      - name: Shipwreck Shoals
        url: "/audio/bfp2/soundtrack/16-Shipwreck-Shoals.mp3"
      - name: Evening Sea
        url: "/audio/bfp2/soundtrack/17-Evening-Sea.mp3"
---
