---
title: Pirates 2 Classes
categories:
  - Battlefield 2
  - Pirates 2
type: mod
layout: layouts/details.njk
preface:
  - name: Peglegs
    icon: pirate-2.svg
    description: "The human Peglegs."
  - name: Skullclan
    icon: pirate-1.svg
    description: "The undead Skull Clan."
details:
  - name: Kits
    icon: pirate-3.svg
    items:
      - name: Sharpshooter
        image:
          - img/game/bfp2/kits/pl_sharpshooter.png
          - img/game/bfp2/kits/ud_sharpshooter.png
        points:
          - name: Cutlass
            image:
              - img/game/bfp2/weapons/handheld/icons/cutlass1.png
              - img/game/bfp2/weapons/handheld/icons/cutlass2.png
            link: ../weapons/#cutlass
          - name: French Pistol
            image: img/game/bfp2/weapons/handheld/icons/sfrenchpistol.png
            link: ../weapons/#pistolette-french-pistol
          - name: Sniper Musket
            image: img/game/bfp2/weapons/handheld/icons/snipermusket.png
            link: ../weapons/#sharpshooter%E2%80%99s-musket
          - name: Smoke Grenade
            image: img/game/bfp2/weapons/utility/icons/smokejar.png
            link: ../weapons/#smoke-grenade
          - name: Spyglass
            image: img/game/bfp2/weapons/utility/icons/spyglass.png
            link: ../weapons/#spyglass
        description: "This class carries a musket rigged with a specially designed scope handcrafted for the king's son, but pillaged from a crown vessel. The Sharpshooter stays in the shadows and avoids the thick of battle, best situated in a prone position. He is also armed with a small pistol and a noxious smoke jar for backup."
        addendum:
          - "Shots will fall rapidly at long distances."
          - "Smoke will obscure enemies vision."
      - name: Raider
        image:
          - img/game/bfp2/kits/pl_raider.png
          - img/game/bfp2/kits/ud_raider.png
        points:
          - name: Cutlass
            image:
              - img/game/bfp2/weapons/handheld/icons/cutlass1.png
              - img/game/bfp2/weapons/handheld/icons/cutlass2.png
            link: ../weapons/#cutlass
          - name: Pistol
            image: img/game/bfp2/weapons/handheld/icons/piratepistol.png
            link: ../weapons/#pistol
          - name: Musket
            image: img/game/bfp2/weapons/handheld/icons/musket.png
            link: ../weapons/#musket
          - name: Grenade
            image: img/game/bfp2/weapons/handheld/icons/grenade.png
            link: ../weapons/#grenade
          - name: Smoke Grenade
            image: img/game/bfp2/weapons/utility/icons/smokejar.png
            link: ../weapons/#smoke-grenade
          - name: Spyglass
            image: img/game/bfp2/weapons/utility/icons/spyglass.png
            link: ../weapons/#spyglass
        description: "The Raider forms the backbone of every crew's fighting forces. Armed with rudimentary grenades, extra ammunition and a heavy pistol, this class is armed for battle. Like every class, the player has the option of drawing a cutlass should the first shots miss in close combat."
        addendum:
          - "Smoke will obscure enemies vision."
      - name: Gunner
        image:
          - img/game/bfp2/kits/pl_gunner.png
          - img/game/bfp2/kits/ud_gunner.png
        points:
          - name: Cutlass
            image:
              - img/game/bfp2/weapons/handheld/icons/cutlass1.png
              - img/game/bfp2/weapons/handheld/icons/cutlass2.png
            link: ../weapons/#cutlass
          - name: French Pistol
            image: img/game/bfp2/weapons/handheld/icons/sfrenchpistol.png
            link: ../weapons/#pistolette-french-pistol
          - name: Musket
            image: img/game/bfp2/weapons/handheld/icons/musket.png
            link: ../weapons/#musket
          - name: Powder Keg
            image: img/game/bfp2/weapons/utility/icons/powderkeg.png
            link: ../weapons/#powder-keg
          - name: Ammo Bag
            image: img/game/bfp2/weapons/utility/icons/ammobaghorn.png
            link: ../weapons/#ammunition-bag
          - name: Spyglass
            image: img/game/bfp2/weapons/utility/icons/spyglass.png
            link: ../weapons/#spyglass
        description: "This scurvy dog is best suited for extreme close quarters combat. Wielding the very devastating blunderbuss, you'd best take an enemy gunner out at range or turn tail and run fer yer life! The class is also armed with a small pistol, and exploding powderkeg, and spare powder staches which can be tossed on the ground to replenish ammunnition of other crewmates."
        addendum:
          - "One powder keg per kit."
      - name: Carpenter
        image:
          - img/game/bfp2/kits/pl_carpenter.png
          - img/game/bfp2/kits/ud_carpenter.png
        points:
          - name: Cutlass
            image:
              - img/game/bfp2/weapons/handheld/icons/cutlass1.png
              - img/game/bfp2/weapons/handheld/icons/cutlass2.png
            link: ../weapons/#cutlass
          - name: French Pistol
            image: img/game/bfp2/weapons/handheld/icons/sfrenchpistol.png
            link: ../weapons/#pistolette-french-pistol
          - name: Musket
            image: img/game/bfp2/weapons/handheld/icons/musket.png
            link: ../weapons/#musket
          - name: Grenade Launcher
            image: img/game/bfp2/weapons/handheld/icons/grenadelauncher.png
            link: ../weapons/#grenade-launcher
          - name: Hammer
            image: img/game/bfp2/weapons/utility/icons/hammer.png
            link: ../weapons/#hammer
          - name: Spyglass
            image: img/game/bfp2/weapons/utility/icons/spyglass.png
            link: ../weapons/#spyglass
        description: "A true mariner at heart, this class is equipped with a short barreled musketoon and a grenade-launching hand cannon. Most importantly, the carpenter carries a hammer which can be used to repair vehicles, stationary weapons, and even the gate on Storm The Bastion."
      - name: Surgeon
        image:
          - img/game/bfp2/kits/pl_surgeon.png
          - img/game/bfp2/kits/ud_surgeon.png
        points:
          - name: Cutlass
            image:
              - img/game/bfp2/weapons/handheld/icons/cutlass1.png
              - img/game/bfp2/weapons/handheld/icons/cutlass2.png
            link: ../weapons/#cutlass
          - name: Pistol
            image: img/game/bfp2/weapons/handheld/icons/piratepistol.png
            link: ../weapons/#pistol
          - name: Musket
            image: img/game/bfp2/weapons/handheld/icons/musket.png
            link: ../weapons/#musket
          - name: Flame Potion Jar
            image: img/game/bfp2/weapons/utility/icons/potionjar.png
            link: ../weapons/#flaming-potionjar
          - name: Grog
            image: img/game/bfp2/weapons/utility/icons/medicgrogkeg.png
            link: ../weapons/#surgeon-toolkit-and-grog-keg
          - name: Spyglass
            image: img/game/bfp2/weapons/utility/icons/spyglass.png
            link: ../weapons/#spyglass
        description: "Every slaty bilge rat depends on a friendly surgeon to tend to his wounds. This class is armed with a deployable grog keg to ease the pain, but also comes with a musket, pistol, and a very nasty flame potion jar which can stick to any surface and cough up noxious green flames."
      - name: Scout
        image:
          - img/game/bfp2/kits/pl_scout.png
          - img/game/bfp2/kits/ud_scout.png
        points:
          - name: Cutlass
            image:
              - img/game/bfp2/weapons/handheld/icons/cutlass1.png
              - img/game/bfp2/weapons/handheld/icons/cutlass2.png
            link: ../weapons/#cutlass
          - name: French Pistol
            image: img/game/bfp2/weapons/handheld/icons/sfrenchpistol.png
            link: ../weapons/#pistolette-french-pistol
          - name: Ducksfoot Pistol
            image: img/game/bfp2/weapons/handheld/icons/ducksfoot.png
            link: ../weapons/#ducksfoot-pistol
          - name: Explosive Crab
            image: img/game/bfp2/weapons/handheld/icons/crab4.png
            link: ../weapons/#crab
          - name: Flare Launcher
            image: img/game/bfp2/weapons/utility/icons/flarelauncher.png
            link: ../weapons/#flare-launcher
          - name: Grappling Hook
            image: img/game/bfp2/weapons/utility/icons/grapplinghook.png
            link: ../weapons/#grappling-hook
          - name: Spyglass
            image: img/game/bfp2/weapons/utility/icons/spyglass.png
            link: ../weapons/#spyglass
        description: "This lightly armed class is best suited for adventuring behind enemy lines, using the crab explosive and a blinding flare launcher to disrupt and bewilder the enemy at its heart. The fire barreled ducksfoot packs a punch across a broad arc to help get him out of most sticky situations. The class may also be outfitted with a grappling hook."
---
