---
title: Battlefield Pirates
layout: layouts/home.njk
games:
  - title: Battlefield Pirates 1942
    url: "/battlefield-pirates"
    image: "/img/bfp.jpg"
    alt: More information about the Battlefield Pirates 1942 mod
  - title: Battlefield Pirates 2
    url: "/battlefield-pirates-2"
    image: "/img/bfp2.jpg"
    alt: More information about the Battlefield Pirates 2 mod
communities:
  - title: HelloClan
    url: "https://helloclan.eu/"
    image: "/img/community/helloclan.jpg"
    description: 'Plays Battlefield Vietnam and 1942 actively, and regularly hosts events for Pirates 1, see their <a href="https://forum.helloclan.eu/forums/473/">events-forum</a>.'
    alt: The HelloClan community regularly hosts events on their servers
  - title: Lost-Soldiers
    url: "https://lost-soldiers.org/"
    image: "/img/community/lost-soldiers.png"
    description: 'Plays Battlefield 2 actively, and regularly hosts events for Pirates 2, see their <a href="https://www.lost-soldiers.org/v2.php?site=forum&board=39">custom maps-forum</a>.'
    alt: The Lost-Soldiers community regularly hosts events on their servers
downloads:
  - title: Battlefield Pirates 1942
    items:
      - title: BFP1 Base Mod
        url: "https://www.moddb.com/games/battlefield-1942/downloads/bf1942-pirates-final-mod"
        image: /img/downloads/bfp1_banner_small.png
        description: Pirates 1 full base mod on ModDB
        alt: Download the full Pirates 1 base mod on ModDB
      - title: BFP1 Anniversary Mappack
        url: "https://www.gamefront.com/games/battlefield-1942/file/battlefield-pirates-1-anniversary-mappack"
        image: "/img/downloads/bfp1_anniversary_mappack.png"
        description: "Additional maps by Scurvy Dogs Clan on GameFront"
        alt: Download the additional maps by Scurvy Dogs Clan on GameFront
  - title: Battlefield Pirates 2
    items:
      - title: BFP2 Base Mod
        url: "https://www.moddb.com/mods/battlefield-pirates-2/downloads"
        image: "/img/downloads/bfp2_banner_small.png"
        description: "Pirates 2 full base mod on ModDB"
        alt: Download the full Pirates 2 base mod on ModDB
---

The Pirates mod is a full conversion of Battlefield 1942 and 2, where the Skull Clan and the Peglegs battle for control.

Brace yourself for a fun-filled adventure on the high seas!

This site is an effort to document information about the two mods in an accessible format.
