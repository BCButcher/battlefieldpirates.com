---
title: Pirates 1942 Weapons
categories:
  - Battlefield 1942
type: mod
layout: layouts/details.njk
grid: "halves thirds"
details:
  - name: Handheld
    icon: musket.svg
    items:
      - name: Cutlass
        image: img/game/bfp1/weapons/handheld/cutlass/main.png
        points:
          - High Accuracy
          - Short Projector Distance
          - No Ammunition
          - No Zoom
          - Medium Power
        description: "Utilizing a cutlass is very difficult for most as it's an interesting weapon. In most games, the sword or dagger is the least used due to the A.I.'s sense of perception. People can try to sneak up on a bot in the Battlefield 2 series, but somehow, the bots can find people easily. In BFP, you can only fight other players. Shorthand melee is very common for the duration of the game.\n\nThe only real way to avoid using the cutlass is killing everyone with one hit with a gun. And the odds of that are not likely."
        addendum:
          - "Long reach."
          - "Crucial to gameplay. Those that don't use it are handicapping themselves."
      - name: Tankard
        image: img/game/bfp1/weapons/handheld/tankard/main.png
      - name: Musket
        image: img/game/bfp1/weapons/handheld/musket/main.png
        points:
          - High Accuracy
          - Very Far Projector Distance
          - 1 Bullet Per Shot, 5 Clips
          - Zoom
          - Medium Power
        description: "The Musket is perfect for distant encounters. Although it's hard to aim closer, it's not impossible. This makes the Musket one of the primary weapons used in BFP."
      - name: Blunderbuss
        image: img/game/bfp1/weapons/handheld/blunderbuss/main.png
        points:
          - Very Low Accuracy
          - Low Projector Distance
          - 20 Bullets Per Shot, 5 Clips
          - Zoom
          - Medium-Strong Power
        description: "One of the most popular ways to use this gun is to have your sword drawn out. When you get close to someone, switch to your blunder and fire."
        addendum:
          - '"Blundersniping" is a common term when a player fires their blunder at an enemy player from a distance. From a distance, this weapon will not be very effective and will only alert the player of enemy presence.'
      - name: Musketoon
        image: img/game/bfp1/weapons/handheld/musket/main.png
        points:
          - High Accuracy
          - Medium to Far Projector Distance
          - 1 Bullets Per Shot, 5 Clips
          - No Zoom
          - Slightly Higher than Medium Power
        description: "The Musketoon is a very roundabout weapon. It seems a little easier to shoot people in short range than the musket does. But since it doesn't have a zoom, most users are forced to close combat if they cannot aim manually (with no scope)."
        addendum:
          - "The Musketoon is the Musket's sister weapon. Instead of attaching a scope, more power was focused into the making of this gun. Although the extra power is only very slightly, it tends to make up for it's lack of a scope."
      - name: Pistol
        image: img/game/bfp1/weapons/handheld/pistol/main.png
        points:
          - Medium-High Accuracy
          - Very Far Projector Distance
          - 1 Bullets Per Shot, 5 Clips
          - Zoom
          - Medium Power
        description: 'Most people like to use the Pistol in order to "finish off" a player they have shot. Shoot someone with the musket, then quickly pull out your pistol to possibly kill him. If that fails, pull out the cutlass and prepare to dual.'
      - name: Ducksfoot Pistol
        image: img/game/bfp1/weapons/handheld/ducksfoot/main.png
        points:
          - 5 Bullets Per Shot
      - name: Throwing Knife
        image: img/game/bfp1/weapons/handheld/throwingknife/main.png
      - name: Stone Axe
        image: img/game/bfp1/weapons/handheld/stoneaxe/main.png
      - name: Grenade
        image: img/game/bfp1/weapons/utility/grenade/main.png
  - name: Utilities
    icon: spyglass.svg
    items:
      - name: 8-pounder
        image: img/game/bfp1/weapons/utility/8pdr/main.png
      - name: Powder Keg
        image: img/game/bfp1/weapons/utility/powderkeg/main.png
      - name: Wrench
        image: img/game/bfp1/weapons/utility/wrench/main.png
      - name: Spyglass
        image: img/game/bfp1/weapons/utility/spyglass/main.png
        description: "The Spy Glass, or monocular, is available to several classes and allows ye to spot enemies on the horizon, and call in bombardment."
      - name: Gate
        image: img/game/bfp1/stationary/gate/main.png
        description: "Some gate doors can be controlled."
  - name: Cannons
    icon: cannon.svg
    items:
      - name: Falconet
        image: img/game/bfp1/stationary/falconet/main.png
        description: "Antipersonnel cannon that is extremely accurate, has only a slight arc, and maintains great range. Can swivel 360 degrees."
      - name: 10-pounder Tower
        image: img/game/bfp1/stationary/10pdr-tower/main.png
        description: "A 3-cannon tower that fires 10-pound cannonballs."
      - name: 12-pounder Tower
        image: img/game/bfp1/stationary/12pdr-tower/main.png
        description: "A 4-cannon tower that fires 12-pound cannonballs."
      - name: 24-pounder Cannon
        image: img/game/bfp1/stationary/24pdr/main.png
        description: "A bigger cannon, with a large arc and not so great range. As such its accuracy suffers, but it has a nice blast radius and on many maps (like Cut Throat Creek) it can dominate a map."
      - name: Fortress Cannon
        image: img/game/bfp1/stationary/fortresscannon/main.png
        description: "Rare but deadly, these are mainly anti-ship cannons, and as such are found on ship maps. Longest-ranged cannon around, accurate and has little arc."
      - name: Grenade Mortar
        image: img/game/bfp1/stationary/grenademortar/main.png
        description: "Seldom used, this mortar drops an exploding timed grenade where you want it. Huge arc (it is a mortar) and short range, but when used correctly, they can make an area of the map uninhabitable."
      - name: Balloon Buster
        image: img/game/bfp1/stationary/balloonbuster/main.png
        description: "Fires multiple shots against balloons."
---
