---
title: Pirates Gameplay
categories:
  - Battlefield 1942
  - Pirates
type: gallery
layout: layouts/gallery.njk
sections:
  - name: Maps
    items:
      - name: Black Sand Buccaneers
        image: img/game/bfp1/gameplay/black-sand-buccaneers/naval-battle.jpg
      - name: Dead Calm
        image: img/game/bfp1/gameplay/dead-calm/drunkard-massacre.jpg
      - name: King Of The Caribbean
        image:
          - img/game/bfp1/gameplay/king-of-the-caribbean/ship-sinking1.jpg
          - img/game/bfp1/gameplay/king-of-the-caribbean/ship-sinking2.jpg
          - img/game/bfp1/gameplay/king-of-the-caribbean/ship-sinking3.jpg
      - name: Azure Seas
        image: img/game/bfp1/gameplay/azure-seas/komodo.png
      - name: Sanctuary
        image: img/game/bfp1/gameplay/sanctuary/searching-for-ropes.png
        description: Searching for ropes to climb up to the Sanctuary
      - name: Two Forts
        image:
          - img/game/bfp1/gameplay/two-forts/anchored-galleon.jpg
          - img/game/bfp1/gameplay/two-forts/artillery.jpg
        description: Mr. Wilson hides in the castle walls
      - name: Tortuga
        image: img/game/bfp1/gameplay/tortuga/mr-wilson.png
        description: Mr. Wilson hides in the castle walls
      - name: Scurvy Cove
        image: img/game/bfp1/gameplay/scurvy-cove/magic-barrel.jpg
  - name: Other
    items:
      - name: Mr. Wilson
        image: img/game/bfp1/gameplay/mr-wilson.png
        description: Mr. Wilson hides on the beach
---
