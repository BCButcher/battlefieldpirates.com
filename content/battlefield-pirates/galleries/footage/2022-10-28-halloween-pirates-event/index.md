---
title: Halloween Event 2022
description: "2022-10-28"
categories:
  - Battlefield 1942
  - Pirates
origin: https://web.archive.org/web/20240922104140/https://forum.helloclan.eu/threads/10091/
image: img/community/bfp1/events/2022-10-28+30.png
type: exhibit
layout: layouts/exhibit.njk
items:
  - name: Battlefield 1942 Pirates Halloween event from  28th of October 2022 part 1
    image: img/game/bfp1/video/youtube-uPqIZ2vnC2E.webp
    link: "https://www.youtube.com/watch?v=uPqIZ2vnC2E"
    publisher: E3gaming
    published: "2022-10-29"
  - name: Battlefield 1942 Pirates Halloween event from  28th of October 2022 part 1
    image: img/game/bfp1/video/youtube-1Luzoqyj_Ds.webp
    link: "https://www.youtube.com/watch?v=1Luzoqyj_Ds"
    publisher: E3gaming
    published: "2022-10-29"
  - name: October 28, 2022 pirate event map BFP TWO FORTS
    image: img/game/bfp1/video/rumble-v1qpdlo.jpg
    link: "https://rumble.com/v1qpdlo-october-28-2022-pirate-event-map-bfp-two-forts.html"
    publisher: Winbean winlite
    published: "2022-10-28"
  - name: October 28, 2022 pirate event map BFP TORTUGA
    image: img/game/bfp1/video/rumble-v1qig5j.jpg
    link: "https://rumble.com/v1qig5j-october-28-2022-pirate-event-map-bfp-tortuga.html"
    publisher: Winbean winlite
    published: "2022-10-28"
  - name: Battlefield 1942, October 28, 2022 pirate event map BFP SANCTUARY
    image: img/game/bfp1/video/rumble-v1qi1rr.jpg
    link: "https://rumble.com/v1qi1rr-october-28-2022-pirate-event-map-bfp-sanctuary.html"
    publisher: Winbean winlite
    published: "2022-10-28"
  - name: Battlefield 1942, October 28, 2022 pirate event map BFP MAROONED
    image: img/game/bfp1/video/rumble-v1qfo1n.jpg
    link: "https://rumble.com/v1qfo1n-october-28-2022-pirate-event-map-bfp-marooned.html"
    publisher: Winbean winlite
    published: "2022-10-28"
  - name: Battlefield 1942, October 28, 2022 pirate event map BFP IMMINENT EXPLOSION
    image: img/game/bfp1/video/rumble-v1qf48l.jpg
    link: "https://rumble.com/v1qf48l-october-28-2022-pirate-event-map-bfp-imminent-explosion.html"
    publisher: Winbean winlite
    published: "2022-10-28"
  - name: Battlefield 1942, October 28, 2022 pirate event map BFP BLACK SAND BUCCANEERS
    image: img/game/bfp1/video/rumble-v1qet41.jpg
    link: "https://rumble.com/v1qet41-october-28-2022-pirate-event-map-bfp-black-sand-buccaneers.html"
    publisher: Winbean winlite
    published: "2022-10-28"
  - name: Battlefield 1942  October 28, 2022 pirate event map BFP FRYLAR
    image: img/game/bfp1/video/rumble-v1qen15.jpg
    link: "https://rumble.com/v1qen15-battlefield-1942-october-28-2022-pirate-event-map-bfp-frylar.html"
    publisher: Winbean winlite
    published: "2022-10-28"
---
