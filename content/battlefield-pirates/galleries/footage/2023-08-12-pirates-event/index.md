---
title: August 2023
description: "2023-08-12"
categories:
  - Battlefield 1942
  - Pirates
origin: https://web.archive.org/web/20240922102906/https://forum.helloclan.eu/threads/10227/
image: img/community/bfp1/events/2023-08-12.jpg
type: exhibit
layout: layouts/exhibit.njk
items:
  - name: Epic Naval Duels and Swashbuckling Action - Battlefield Pirate Mod
    image: img/game/bfp1/video/youtube-UqLDn2N89VY.jpg
    link: "https://www.youtube.com/watch?v=UqLDn2N89VY"
    publisher: Winbean winlite
    published: "2023-08-15"
  - name: "Land and Sea Clash: Battlefield Pirates in an Epic Dual Battle!"
    image: img/game/bfp1/video/youtube-WzXk8KOEtak.jpg
    link: "https://www.youtube.com/watch?v=WzXk8KOEtak"
    publisher: Winbean winlite
    published: "2023-08-15"
  - name: "Pirate Mod Adventures: Pillaging and Plundering in Battlefield 1942!"
    image: img/game/bfp1/video/youtube-tSSKPQ0NpAQ.jpg
    link: "https://www.youtube.com/watch?v=tSSKPQ0NpAQ"
    publisher: Winbean winlite
    published: "2023-08-15"
  - name: "Hoist the Jolly Roger: Battlefield Pirate Mod Night Raid"
    image: img/game/bfp1/video/youtube-TF_lYglWLiA.jpg
    link: "https://www.youtube.com/watch?v=TF_lYglWLiA"
    publisher: Winbean winlite
    published: "2023-08-15"
  - name: "Sky-High Adventures: Battlefield Pirates Take to the Skies in Air Balloons!"
    image: img/game/bfp1/video/youtube-5lFkq4buLv0.jpg
    link: "https://www.youtube.com/watch?v=5lFkq4buLv0"
    publisher: Winbean winlite
    published: "2023-08-15"
  - name: "Unleashing Chaos on the High Seas: Epic Battlefield Pirates Sea Battle!"
    image: img/game/bfp1/video/youtube-abEJ9yVE6LI.jpg
    link: "https://www.youtube.com/watch?v=abEJ9yVE6LI"
    publisher: Winbean winlite
    published: "2023-08-15"
  - name: "Unleash Chaos Under the Moonlight: Battlefield Pirate Mod Night Gameplay"
    image: img/game/bfp1/video/youtube-jkkFbkec9HA.jpg
    link: "https://www.youtube.com/watch?v=jkkFbkec9HA"
    publisher: Winbean winlite
    published: "2023-08-15"
---
