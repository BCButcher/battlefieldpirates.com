---
title: Caribbean Invasion Pirates Event
description: "2024-06-08"
categories:
  - Battlefield 1942
  - Pirates
origin: https://web.archive.org/web/20240922093614/https://forum.helloclan.eu/threads/10425/
image: img/community/bfp1/events/2024-06-08.jpg
type: exhibit
layout: layouts/exhibit.njk
items:
  - name: "Battlefield Pirates Compilation (Battlefield 1942 Mod)"
    image: img/game/bfp1/video/youtube-19N_yyPVdLQ.webp
    link: "https://www.youtube.com/watch?v=19N_yyPVdLQ"
    publisher: Laska
    published: "2024-06-22"
  - name: "BF Pirates Imminent Explosion 002"
    image: img/game/bfp1/video/youtube-Pe7DA5B3eX0.webp
    link: "https://www.youtube.com/watch?v=Pe7DA5B3eX0"
    publisher: penske83
    published: "2024-06-14"
  - name: "BF Pirates Imminent Explosion 001"
    image: img/game/bfp1/video/youtube-9zXwIYNCZis.webp
    link: "https://www.youtube.com/watch?v=9zXwIYNCZis"
    publisher: penske83
    published: "2024-06-11"
  - name: "BF1942 ⧸ BF Pirates： Crossing the Planks - A Battle of Bridges"
    image: img/game/bfp1/video/rumble-v4yjxai.jpg
    link: "https://rumble.com/v50w6dr-bf1942-bf-pirates-crossing-the-planks-a-battle-of-bridges.html"
    publisher: Winbean45
    published: "2024-06-11"
  - name: "Battlefield 1942 ⧸ Battlefield Pirates： The Skull Clan Returns to Haunt Scurvy Cove!"
    image: img/game/bfp1/video/rumble-v4yo1bf.jpg
    link: "https://rumble.com/v510acl-battlefield-1942-battlefield-pirates-the-skull-clan-returns-to-haunt-scurvy.html"
    publisher: Winbean45
    published: "2024-06-11"
  - name: "BF1942 ⧸ BF Pirates： Foggy Shores - A Beached Map Adventure"
    image: img/game/bfp1/video/rumble-v4yjvke.jpg
    link: "https://rumble.com/v50w4nn-bf1942-bf-pirates-foggy-shores-a-beached-map-adventure.html"
    publisher: Winbean45
    published: "2024-06-11"
  - name: "Battlefield 1942 ⧸Battlefield Pirates： BFP Two Forts - Island Fort Slugfest"
    image: img/game/bfp1/video/rumble-v4ymlar.jpg
    link: "https://rumble.com/v50yud6-battlefield-1942-battlefield-pirates-bfp-two-forts-island-fort-slugfest..html"
    publisher: Winbean45
    published: "2024-06-11"
  - name: "Battlefield Pirates - Carribean Invasion HelloClan event / Трансляция"
    image: img/game/bfp1/video/youtube-_ko8m2SoAFk.webp
    link: "https://www.youtube.com/watch?v=_ko8m2SoAFk"
    publisher: LastofAvari Belarus
    published: "2024-06-08"
---
