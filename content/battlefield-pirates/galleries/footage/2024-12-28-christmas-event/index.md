---
title: Christmas Event 2024
description: "2024-12-28"
categories:
  - Battlefield 1942
  - Pirates
origin: https://web.archive.org/web/20241229094101/https://forum.helloclan.eu/threads/10537/
image: img/community/bfp1/events/2024-12-28.jpg
type: exhibit
layout: layouts/exhibit.njk
items:
  - name: "Battlefield 1942 /battlefield pirates1: Livestream"
    image: img/game/bfp1/video/rumble-v61i89j.jpg
    link: "https://rumble.com/v63qnad-battlefield-1942-pirates-livestream-12282024.html"
    publisher: Winbean45-GAMING
    published: "2024-12-28"
---
