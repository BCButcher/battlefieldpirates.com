---
title: Cool Dawn Event
description: "2025-02-15"
categories:
  - Battlefield 1942
  - Pirates
origin: https://web.archive.org/web/20250218193306/https://forum.helloclan.eu/threads/10562/
image: img/community/bfp1/events/2024-12-28.jpg
type: exhibit
layout: layouts/exhibit.njk
items:
  - name: "Battlefield 1942 Pirates Mod ONLINE in 2025 - Cut Throat Creek Capture the Flag - No Commentary"
    image: img/game/bfp1/video/youtube-WC4V9buDXhc.webp
    link: "https://www.youtube.com/watch?v=WC4V9buDXhc"
    publisher: TheCalmingClam
    published: "2025-02-16"
---
