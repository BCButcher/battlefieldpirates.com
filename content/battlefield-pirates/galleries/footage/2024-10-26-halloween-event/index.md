---
title: Halloween Event 2024
description: "2024-10-26"
categories:
  - Battlefield 1942
  - Pirates
origin: https://web.archive.org/web/20240922171615/https://forum.helloclan.eu/threads/10501/
image: img/community/bfp1/events/2024-10-26.jpg
type: exhibit
layout: layouts/exhibit.njk
items:
  - name: "Battlefield 1942 /battlefield pirates1: Livestream"
    image: img/game/bfp1/video/rumble-v5hzkm5.jpg
    link: "https://rumble.com/v5k8ptx-battlefield-1942-battlefield-pirates1-livestream-10262024.html"
    publisher: Winbean45-GAMING
    published: "2024-10-26"
---
