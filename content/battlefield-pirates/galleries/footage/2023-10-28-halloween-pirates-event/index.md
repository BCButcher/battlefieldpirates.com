---
title: Halloween Event 2023
description: "2023-10-28"
categories:
  - Battlefield 1942
  - Pirates
origin: https://web.archive.org/web/20240922102719/https://forum.helloclan.eu/threads/10260/
image: img/community/bfp1/events/2023-10-28.jpg
type: exhibit
layout: layouts/exhibit.njk
items:
  - name: "Battlefield Pirates 2 /// Личное Мнение"
    image: img/game/bfp1/video/youtube-zaMO24T0Z-s.webp
    link: "https://www.youtube.com/watch?v=zaMO24T0Z-s"
    publisher: LastofAvari Belarus
    published: "2023-10-28"
---
