---
title: September 2023 Pirates Event
description: "2023-09-23"
categories:
  - Battlefield 1942
  - Pirates
origin: https://web.archive.org/web/20240922102809/https://forum.helloclan.eu/threads/10239/
image: img/community/bfp1/events/2023-09-23.jpg
type: exhibit
layout: layouts/exhibit.njk
items:
  - name: "BF Pirates - Shiver Me Timbers: High Seas Adventures in Battlefield 1942"
    image: img/game/bfp1/video/youtube-ko6EU--f7mI.webp
    link: "https://www.youtube.com/watch?v=ko6EU--f7mI"
    publisher: Winbean winlite
    published: "2023-09-27"
  - name: HelloClan Pirates Event September 2023 - One Bridge Too Far
    image: img/game/bfp1/video/youtube-F-9dPMtizZU.jpg
    link: "https://www.youtube.com/watch?v=F-9dPMtizZU"
    publisher: Winbean winlite
    published: "2023-09-23"
---
