---
title: Flags and Swords Pirates Event
description: "2024-02-17"
categories:
  - Battlefield 1942
  - Pirates
origin: https://web.archive.org/web/20240922094152/https://forum.helloclan.eu/threads/10339/
image: img/community/bfp1/events/2024-02-17.jpg
type: exhibit
layout: layouts/exhibit.njk
items:
  - name: "Clash of the Undead： The Imminent Explosion at Skull Island"
    image: img/game/bfp1/video/rumble-v4euyex.jpg
    link: "https://rumble.com/v4euyex-clash-of-the-undead-the-imminent-explosion-at-skull-island.html"
    publisher: Winbean45
    published: "2024-02-21"
  - name: "Timberment： Island of the Wooden Fortresses"
    image: img/game/bfp1/video/rumble-v4exw69.jpg
    link: "https://rumble.com/v4exw69-timberment-island-of-the-wooden-fortresses.html"
    publisher: Winbean45
    published: "2024-02-21"
  - name: "Battlefield Pirates： The Ultimate Skeleton vs. Human Showdown on Paradise Island!"
    image: img/game/bfp1/video/rumble-v4f0nw3.jpg
    link: "https://rumble.com/v4f0nw3-battlefield-pirates-the-ultimate-skeleton-vs.-human-showdown-on-paradise-is.html"
    publisher: Winbean45
    published: "2024-02-22"
  - name: "Battlefield Pirates： Pegleg's Retirement at Stake in Skullclan Showdown at Tortuga!"
    image: img/game/bfp1/video/rumble-v4f0srx.jpg
    link: "https://rumble.com/v4f0srx-battlefield-pirates-peglegs-retirement-at-stake-in-skullclan-showdown-at-to.html"
    publisher: Winbean45
    published: "2024-02-22"
  - name: ""
    image: img/game/bfp1/video/rumble-v4f0qbk.jpg
    link: "https://rumble.com/v4f0qbk-battlefield-pirates-epic-castle-clash-in-skeleton-vs.-human-showdown.html"
    publisher: Winbean45
    published: "2024-02-21"
  - name: "Battlefield Pirates： Galleon Showdown in the High-Stakes Battle for King of the Caribbean!"
    image: img/game/bfp1/video/rumble-v4f4x3f.jpg
    link: "https://rumble.com/v4f4x3f-battlefield-pirates-galleon-showdown-in-the-high-stakes-battle-for-king-of-.html"
    publisher: Winbean45
    published: "2024-02-22"
  - name: "Battlefield Pirates： Shiver Me Timbers - Tropical Island Pillaging & Carnage!"
    image: img/game/bfp1/video/rumble-v4f50zw.jpg
    link: "https://rumble.com/v4f50zw-battlefield-pirates-shiver-me-timbers-tropical-island-pillaging-and-carnage.html"
    publisher: Winbean45
    published: "2024-02-22"
  - name: "BFP Bloody Bayou： Pirates vs. Skeletons - Nighttime Battle on Land"
    image: img/game/bfp1/video/rumble-v4f8sjh.jpg
    link: "https://rumble.com/v4f8sjh-bfp-bloody-bayou-pirates-vs.-skeletons-nighttime-battle-on-land.html"
    publisher: Winbean45
    published: "2024-02-23"
---
