---
title: Pirates Footage
categories:
  - Battlefield 1942
  - Pirates
type: gallery
layout: layouts/gallery.njk
sections:
  - name: 2025
    items:
      - name: Cool Dawn Event
        link: "./2025-02-15-cool-dawn-event/"
        description: "2025-02-15"
        origin: https://web.archive.org/web/20250218193306/https://forum.helloclan.eu/threads/10562/
        image: img/community/bfp1/events/2025-02-15.jpg
  - name: 2024
    items:
      - name: Christmas Event 2024
        link: "./2024-12-28-christmas-event/"
        description: "2024-12-28"
        origin: https://web.archive.org/web/20241229094101/https://forum.helloclan.eu/threads/10537/
        image: img/community/bfp1/events/2024-12-28.jpg
      - name: Halloween Event 2024
        link: "./2024-10-26-halloween-event/"
        description: "2024-10-26"
        origin: https://web.archive.org/web/20240922171615/https://forum.helloclan.eu/threads/10501/
        image: img/community/bfp1/events/2024-10-26.jpg
      - name: Red Horizon Event
        description: "2024-08-24"
        origin: https://web.archive.org/web/20240922093812/https://forum.helloclan.eu/threads/10483/
        image: img/community/bfp1/events/2024-08-24.jpg
      - name: Caribbean Invasion Event
        link: "./2024-06-08-caribbean-invasion-pirates-event/"
        description: "2024-06-08"
        origin: https://web.archive.org/web/20240922093614/https://forum.helloclan.eu/threads/10425/
        image: img/community/bfp1/events/2024-06-08.jpg
      - name: High Action On The High Seas Event
        link: "./2024-04-20-high-action-on-the-high-seas-pirates-event/"
        description: "2024-04-20"
        origin: https://web.archive.org/web/20240922093939/https://forum.helloclan.eu/threads/10379/
        image: img/community/bfp1/events/2024-04-20.jpg
      - name: Flags and Swords Event
        link: "./2024-02-17-flags-and-swords-pirates-event/"
        description: "2024-02-17"
        origin: https://web.archive.org/web/20240922094152/https://forum.helloclan.eu/threads/10339/
        image: img/community/bfp1/events/2024-02-17.jpg
  - name: 2023
    items:
      - name: December 2023
        description: "2023-12-30"
        origin: https://web.archive.org/web/20240922094322/https://forum.helloclan.eu/threads/10297/
        image: img/community/bfp1/events/2023-12-30.jpg
      - name: November 2023
        description: "2023-11-25"
        origin: https://web.archive.org/web/20240922102438/https://forum.helloclan.eu/threads/10282/
        image: img/community/bfp1/events/2023-11-25.jpg
      - name: Halloween Event 2023
        link: "./2023-10-28-halloween-pirates-event/"
        description: "2023-10-28"
        origin: https://web.archive.org/web/20240922102719/https://forum.helloclan.eu/threads/10260/
        image: img/community/bfp1/events/2023-10-28.jpg
      - name: September 2023
        link: "./2023-09-23-pirates-event/"
        description: "2023-09-23"
        origin: https://web.archive.org/web/20240922102809/https://forum.helloclan.eu/threads/10239/
        image: img/community/bfp1/events/2023-09-23.jpg
      - name: August 2023
        link: "./2023-08-12-pirates-event/"
        description: "2023-08-12"
        origin: https://web.archive.org/web/20240922102906/https://forum.helloclan.eu/threads/10227/
        image: img/community/bfp1/events/2023-08-12.jpg
      - name: July 2023
        description: "2023-07-15"
        origin: https://web.archive.org/web/20240922103026/https://forum.helloclan.eu/threads/10207/
        image: img/community/bfp1/events/2023-07-15.jpg
      - name: January 2023
        description: "2023-01-27"
        origin: https://web.archive.org/web/20240922103945/https://forum.helloclan.eu/threads/10143/
        image: img/community/bfp1/events/2023-01-27.jpg
  - name: 2022
    items:
      - name: Halloween Event 2022
        link: "./2022-10-28-halloween-pirates-event/"
        description: "2022-10-28"
        origin: https://web.archive.org/web/20240922104140/https://forum.helloclan.eu/threads/10091/
        image: img/community/bfp1/events/2022-10-28+30.png
      - name: Battlefield 1942 Pirates Mod In 2022
        image: img/game/bfp1/video/youtube-6eEnkRnSIQA.webp
        link: "https://www.youtube.com/watch?v=6eEnkRnSIQA"
        publisher: Always Battlefield
        published: "2022-06-24"
      - name: Battlefield 1942 - HelloClan Pirates Event Music Video
        image: img/game/bfp1/video/youtube-E2GKvayNPWo.webp
        link: "https://www.youtube.com/watch?v=E2GKvayNPWo"
        publisher: LuccaWulf
        published: "2022-03-20"
  - name: 2020
    items:
      - name: battlefield Pirates event ：) part 1 in 4k 50 fps
        image: img/game/bfp1/video/youtube-9HZr3DVqaSc.webp
        link: "https://www.youtube.com/watch?v=9HZr3DVqaSc"
        publisher: E3gaming
        published: "2020-04-12"
      - name: Battlefield Pirates event ：) part 2 in 4k 50 fps
        image: img/game/bfp1/video/youtube-bXDx2sDi9t4.webp
        link: "https://www.youtube.com/watch?v=bXDx2sDi9t4"
        publisher: E3gaming
        published: "2020-04-12"
      - name: battlefield Pirates event ：) part 3 in 4k 50 fps this was the last round.
        image: img/game/bfp1/video/youtube-6nlO3fQxUdg.webp
        link: "https://www.youtube.com/watch?v=6nlO3fQxUdg"
        publisher: E3gaming
        published: "2020-04-12"
      - name: Battlefield Pirates  late night fun !
        image: img/game/bfp1/video/youtube-NxIAjZu9W9c.webp
        link: "https://www.youtube.com/watch?v=NxIAjZu9W9c"
        publisher: E3gaming
        published: "2020-01-22"
  - name: 2019
    items:
      - name: playing Battlefield Pirates  with huge pirate ships.
        image: img/game/bfp1/video/youtube-Cf1MmBKiayc.webp
        link: "https://www.youtube.com/watch?v=Cf1MmBKiayc"
        publisher: E3gaming
        published: "2019-10-27"
      - name: Playing Battlefield Pirates  on the komodo dragon
        image: img/game/bfp1/video/youtube-PQcg0JsXzAg.webp
        link: "https://www.youtube.com/watch?v=PQcg0JsXzAg"
        publisher: E3gaming
        published: "2019-10-27"
      - name: The last of the Battlefield   Pirate   videos on  pirate Islands
        image: img/game/bfp1/video/youtube-n6GLyYmtSjc.webp
        link: "https://www.youtube.com/watch?v=n6GLyYmtSjc"
        publisher: E3gaming
        published: "2019-10-26"
      - name: playing Battlefield Pirates yar
        image: img/game/bfp1/video/youtube-k_sTxwmSNqU.webp
        link: "https://www.youtube.com/watch?v=k_sTxwmSNqU"
        publisher: E3gaming
        published: "2019-10-26"
      - name: "Battlefield 1942: Pirates"
        image: img/game/bfp1/video/youtube-wSSA9SDB9Zw.webp
        link: "https://www.youtube.com/watch?v=wSSA9SDB9Zw"
        publisher: LastofAvari Belarus
        published: "2019-10-13"
      - name: "Battlefield 1942: Pirates"
        image: img/game/bfp1/video/youtube-iw5gqzmWgmo.webp
        link: "https://www.youtube.com/watch?v=iw5gqzmWgmo"
        publisher: LastofAvari Belarus
        published: "2019-07-13"
  - name: Older
    items:
      - name: "Battlefield 1942: Pirates"
        image: img/game/bfp1/video/youtube-dS2Zn0J4r98.webp
        link: "https://www.youtube.com/watch?v=dS2Zn0J4r98"
        publisher: LastofAvari Belarus
        published: "2018-10-20"
      - name: "Battlefield: Pirates на сервере THE DRUNKEN MUG"
        image: img/game/bfp1/video/youtube-IK1Eox-Rcl8.webp
        link: "https://www.youtube.com/watch?v=IK1Eox-Rcl8"
        publisher: LastofAvari Belarus
        published: "2018-06-23"
      - name: Battlefield Pirates loading music
        image: img/game/bfp1/video/youtube-X2VNFGmcdkU.jpg
        link: "https://www.youtube.com/watch?v=X2VNFGmcdkU"
        publisher: Haakona
        published: "2010-02-02"
      - name: Battlefield Pirates Menu music
        image: img/game/bfp1/video/youtube-D-2Q_SKZLS8.jpg
        link: "https://www.youtube.com/watch?v=D-2Q_SKZLS8"
        publisher: Haakona
        published: "2010-02-02"
---
