---
title: Pirates Galleries
categories:
  - Battlefield 1942
  - Pirates
type: media
layout: layouts/mod.njk
sections:
  - name: Gameplay
    icon: paddles.svg
    link: "./gameplay"
  - name: Footage
    icon: pirate-flag.svg
    link: "./footage"
---
