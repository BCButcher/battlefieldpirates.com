---
title: Pirates 1942
date: 2005-11-22
description: >-
  Battlefield Pirates is a full conversion mod of Battlefield 1942. The general
  theme of the game is two rivaling clans, the Skull Clan and the Peglegs. These
  two pirate clans have been clashing at each other's throats for years and over
  the course of time, they have evolved with new weaponry to kill one another.
  Brace yourself for a fun-filled adventure on the high seas!
categories:
  - Battlefield 1942
type: mod
layout: layouts/mod.njk
banner: true
sections:
  - name: Maps
    icon: compass.svg
    link: "./maps"
  - name: Classes
    icon: pirate.svg
    link: "./classes"
  - name: Weapons
    icon: sabers.svg
    link: "./weapons"
  - name: Vehicles
    icon: ship.svg
    link: "./vehicles"
  - name: Galleries
    icon: porthole.svg
    link: "./galleries"
  - name: About
    icon: scroll.svg
    link: "./about"
---
