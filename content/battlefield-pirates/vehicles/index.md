---
title: Pirates 1942 Vehicles
categories:
  - Battlefield 1942
type: mod
layout: layouts/details.njk
grid: "halves thirds"
details:
  - name: Ships Of The Line
    icon: ship.svg
    items:
      - name: Galleon
        image: img/game/bfp1/vehicles/galleon/main.png
        points:
          - 8 adjustable cannons on each side
          - Fires a barrage of 12-pound cannonballs
          - "Mobile or stationary spawnpoint."
        description: "The big ship of the game, a spawn and, carrier of lots of firepower. It has falconets on a top mast and some on the ship front. Sometimes it is stationary and only the falconets will be usable."
      - name: Brig
        points:
          - "Mobile or stationary spawnpoint."
        description: "A ship that fits nicely in the void between a galleon and a sloop. It can be a mobile spawn, has a climbable mast and has some swivel 8 lb cannons and falconets."
      - name: Privateer
        image: img/game/bfp1/vehicles/privateer/main.png
        points:
          - 5 adjustable cannons on each side
          - Fires a barrage of 12-pound cannonballs
        description: "Medium ship with side cannons and a powerful cannon at the front."
      - name: Sloop
        image: img/game/bfp1/vehicles/sloop/main.png
        points:
          - 3 adjustable cannons on each side
          - Fires a barrage of 12-pound cannonballs
        description: "Medium ship with side cannons and a swivel falconet."
  - name: Boats
    icon: boat.svg
    items:
      - name: Runabout
        image: img/game/bfp1/vehicles/runabout/main.png
        description: "The speediest ship in the game, it is mainly for troop transport, though it has a falconet to make it a decent small attack boat. It can hold a lot of pirates but can be sunk in only 2 falconet shots."
      - name: Gnat
        image: img/game/bfp1/vehicles/gnat/main.png
        description: "A 1-man dinghy and a powerful 24lb-cannon attached."
      - name: Mosquito
        description: "A 2-man dinghy with a sail and a nifty falconet attached."
      - name: Dinghy
        image: img/game/bfp1/vehicles/dinghy/main.png
        description: "A 2-man rowboat for transport."
      - name: Barrel
        image: img/game/bfp1/vehicles/floatingbarrel/main.png
        description: "A 1-man escape-vessel, these are on galleons and in other places as a life preserver."
  - name: Other
    icon: parrot.svg
    items:
      - name: Balloon
        image: img/game/bfp1/vehicles/balloon/main.png
        description: "A flying, lumbering, bastard of thing, it has several powerful keg bombs that explode on impact as well as falconets to clear the rigging of any ship. It can also hold several pirates to Para-drop."
      - name: Turtle
        image: img/game/bfp1/vehicles/turtle/main.png
        description: "A single man submarine, that is slow as molasses. It packs a powerful bomb inside and self-destructs much to the chagrin of enemies and driver."
      - name: Komodo
        image: img/game/bfp1/vehicles/komodo/main.png
        description: "A little lizard useful for stealth but hard to drive. Its bite can harm foes and it now swims instead of bottom crawls."
---
