---
title: About Pirates
categories:
  - Battlefield 1942
  - Pirates
type: mod
layout: layouts/default.njk
origin: https://web.archive.org/web/20080923155707/http://www.bfpirates.com/wiki/index.php/BFP
---

## Game Modes

Battlefield Pirates supports the Conquest, Capture The Flag, and Team Death Match modes, which are the normal [game modes in Battlefield 1942](https://web.archive.org/web/20230430123358/https://www.ign.com/wikis/battlefield-1942/Multiplayer).

## History

### Alpha 0.11

Released at 2003-06-27, it included the map Scurvy Cove -- later known as Scurvy Cove Classic -- and the later deprecated map Low Tide.

### Alpha 0.23

Released at 2004-02-11, it added the maps High Tide, Komodo Island, Tides of Blood, Domes of Doom, and Blackbeards Booty.

### Beta 3.1

Released at 2004-07-13, it added the maps Black Sand Buccaneers, Cannonball Coast, Cut Throat Creek, Dead Calm, Iron Seas, King of the Caribbean, Mr Coconuts Island Getaway, Mysterious Mountain, Peninsula of Pain, Prison Isle, Shark Haven, Shiver Me Timbers, Tortuga, and Two Forts. Iron Seas was later made into Azure Seas.

### Final 1.0

Released at 2005-11-01, it added the maps Azure Seas, Beached, Bloody Bayou, Broken Alliance, Dire Straits, Frylar, Haunted Cove, Imminent Explosion, Marooned, Sanctuary, Scurvy Cove Classic, Short on Supplies, Teachs Wit, and Uncharted Waters. It also added the Brig, Mosquito, 2-pirate Dinghy, Komodo Dragon (swimming), and Cutter.

## Team

According to the Beta 3.1 installer and Final installer, the staff included the following.

### Development

- Captain Guy Smiley: Creator, Lead Artist, Lead Designer/Mapper, Scripting, Sound
- SupaFly: Artistic Lead, Public Relations
- Brutus: Technical Lead, Mapping Lead
- BunnySnot: Mapping, Modeling, Scripting
- Magnus333: Beta Test Lead, Mapping, Site Resources
- [DICE] Zerk Mapping: Modeling, Scripting
- Kielbasa: Music
- Chacal: Lead Server Admin
- Stealth: Mapping
- Anonymousity: Mapping, Media, Site Admin
- USAF 777: Coder
- Cap'n Crunch: Test Lead
- Gotmilk: Web

### Testers

- Andy
- BBAmigos
- BBKev4000
- Berzerk
- Blind_lemmon_Lipshitz
- BloodY TOM Kidd
- bobthealmighty
- Cannon_Fodder
- Cap'n Crunch
- Capt. Jack Sparrow
- Chacal
- Cigaboo
- CaptainPiccolo
- Cptn Flint
- DarkCloud
- EasyTarget
- Falcon
- Falconeye
- Funky Ed
- Ganjaman
- GexMax
- GotMilk?
- Gunner
- HoratioHornblower
- Jack Rackham
- K9 Carlos
- Kielbasa
- MasternCommander
- MASTERThreepwood
- mistabullet
- Mr.Corpsehy
- Polaris
- RCSI
- Royal_Navy_Commando
- Shnagenburg The Ninja
- stealth161987
- Sully
- Tawxic
- The Fornicator
- Three Year Old
- tibmaker
- Unholymunk
- USAF 777
- Vejito
- Vivi_Orunitia
- Xa4

### Special Thanks

- The Cut Throats Clan
- BBXPack development team
- Detrux
- The Fornicator
- Bunnysnot
- BFPirates Community
- NGM-Movies.de: Background video and trailer
- Afterburn: Original site hosting
- Rex Hill: Creating the export tools
- Dean Cornish: All his programming shortcuts
- Team Warchild: Beta test server hosting
- Tibmaker: Additional terrain textures
- DICE: For creating such a wonderful game
- EA: For helping DICE make the game a reality
