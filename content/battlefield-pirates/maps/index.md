---
title: Pirates 1942 Maps
date: 2005-11-22
description: The original Battlefield 1942 Pirates mod featured 33 maps, and an additional 19 were added with the Anniversary Map Pack. Several of them work equally well as Conquest-maps and Capture The Flag, but they are more divided in their size. Some maps are huge, and combine large-scale naval battles with intense infantry-combat on smaller islands. Some bring in specific strategies and tactics, as well as a broad arsenal of weapons, vehicles, and ways of playing.
categories:
  - Battlefield 1942
type: mod
layout: layouts/maps.njk
---
