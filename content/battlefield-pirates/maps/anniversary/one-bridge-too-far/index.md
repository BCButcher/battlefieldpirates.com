---
title: One Bridge Too Far
author: Muad'Dib
description: A simple task awaits ye here, kill the enemy and take their booty.
date: '2007-09-21'
categories:
  - Battlefield 1942
  - Anniversary
tags:
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - CTF
  size: 1024
  source: anniversary
  controlpoints:
    - name: Blue Main
      id: Blue_Main
      position:
        x: '433.65'
        'y': '90.51'
        z: '384.76'
    - name: Bridges
      id: bridges
      position:
        x: '445.71'
        'y': '88.55'
        z: '467.32'
    - name: Redmain
      id: redmain
      position:
        x: '454.32'
        'y': '90.47'
        z: '547.03'
  created: '2007-09-21'
  creator: Muad'Dib
---

