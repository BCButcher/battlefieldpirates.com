---
title: Pirates Playhouse
author: Brutus
description: >-
  Chute in, ladder up, knock em down! This aint your Mama's Playhouse so get to
  slittin' gizzards!!! Oh don't worry if ya gots lubber's legs, the parachutes
  work inside too.
date: '2007-09-21'
categories:
  - Battlefield 1942
  - Anniversary
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 1024
  source: anniversary
  controlpoints:
    - name: Axis Base
      id: AxisBase
      position:
        x: '459.42'
        'y': '64.15'
        z: '470.07'
    - name: Allied Base
      id: AlliedBase
      position:
        x: '460.46'
        'y': '66.42'
        z: '673.51'
    - name: Playhouse
      id: Playhouse
      position:
        x: '457.81'
        'y': '89.43'
        z: '564.65'
  created: '2007-09-21'
  creator: Brutus
---

