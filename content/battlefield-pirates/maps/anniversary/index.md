---
title: Battlefield Pirates 2007 Anniversary Map Pack
date: 2007-09-21
description: >-
  Around this time two years ago work on Battlefield Pirates (BFP) Final began
  culminating in its release in November 2005 and not a lot has changed since
  then in BFP for BFP1. That is until now! We have brought together some of the
  BFP Final mappers and some of our own mapmakers from within The Scurvy Dogs
  clan to produce this Mappack which we hope will further your enjoyment of BFP1
  and help keep the pirates tradition going. A lot of hard work has been put in
  from some dedicated pirate mappers and testers, so thanks to all involved.


  This pack contains 19 maps, we hope to see you on the battlefield!
categories:
  - Pirates
type: source
layout: layouts/source.njk
origin: >-
  https://web.archive.org/web/20230427061014/https://www.gamefront.com/games/battlefield-1942/file/battlefield-pirates-1-anniversary-mappack
---
