---
title: Cave Raid
author: Brutus
description: >-
  The Peglegs have found the Skullclan's biggest rum distillery! Control the
  island's inner rum castle and drink your fill for days.
date: '2007-09-21'
categories:
  - Battlefield 1942
  - Anniversary
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 1024
  source: anniversary
  controlpoints:
    - name: Grand Distillery
      id: Grand_Distillery
      position:
        x: '577.12'
        'y': '72.88'
        z: '549.23'
    - name: Allied Base
      id: AlliedBase
      position:
        x: '345.10'
        'y': '73.69'
        z: '498.82'
    - name: Spare Lumber
      id: Spare_Lumber
      position:
        x: '495.83'
        'y': '72.43'
        z: '560.93'
    - name: Outer Gate
      id: Outer_Gate
      position:
        x: '471.94'
        'y': '73.72'
        z: '423.86'
    - name: Inner Cannons And Skull Graves
      id: Inner_Cannons_and_Skull_Graves
      position:
        x: '590.90'
        'y': '69.35'
        z: '621.33'
  created: '2007-09-21'
  creator: Brutus
---

