---
title: Two Castles
author: Muad'Dib
description: >-
  Castles exist to keep people out and kill the ones that want to come in. Two
  castles exist to create chaos. Take what you can; give nothing back.
date: '2007-09-21'
categories:
  - Battlefield 1942
  - Anniversary
tags:
  - Conquest
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 1024
  source: anniversary
  controlpoints:
    - name: Pegleg Fortifications
      id: Pegleg_Fortifications
      position:
        x: '511.88'
        'y': '94.44'
        z: '566.98'
    - name: Skullclan Fortifications
      id: Skullclan_Fortifications
      position:
        x: '511.88'
        'y': '94.30'
        z: '457.44'
    - name: Axis Base
      id: AxisBase
      position:
        x: '511.88'
        'y': '99.50'
        z: '350.40'
    - name: Allied Base
      id: AlliedBase
      position:
        x: '511.88'
        'y': '99.47'
        z: '678.94'
  created: '2007-09-21'
  creator: Muad'Dib
---

