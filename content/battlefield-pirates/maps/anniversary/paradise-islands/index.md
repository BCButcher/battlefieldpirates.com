---
title: Paradise Islands
author: ''
description: >-
  These islands were surely the best place to live on the world, untill the day
  the skulls went out of their graves, and made the paradise turned into hell.
  And now each clan is trying to get the other one down at any price.
date: ''
categories:
  - Battlefield 1942
  - Anniversary
tags:
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - CTF
  size: 1024
  source: anniversary
  controlpoints:
    - name: Skull Clan Flag
      id: SkullClan_Flag
      position:
        x: '519.49'
        'y': '52.50'
        z: '470.80'
    - name: Peg Legs Flag
      id: PegLegs_Flag
      position:
        x: '656.00'
        'y': '46.00'
        z: '610.00'
---

