---
title: Isle Of Remore
author: Stealth
description: >-
  Remore as in 'Re + More.' Pegs fight Skullclan and stuff. Who even reads
  these?
date: '2007-09-21'
categories:
  - Battlefield 1942
  - Anniversary
tags:
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - CTF
  size: 1024
  source: anniversary
  controlpoints:
    - name: Axis Base
      id: AxisBase
      position:
        x: '342.31'
        'y': '99.71'
        z: '420.21'
    - name: Allied Base
      id: AlliedBase
      position:
        x: '483.46'
        'y': '100.08'
        z: '679.48'
  created: '2007-09-21'
  creator: Stealth
---

