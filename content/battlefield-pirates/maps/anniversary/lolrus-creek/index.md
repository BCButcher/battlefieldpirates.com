---
title: Lolrus Creek
author: Cap'n Pugwash
description: >-
  Despite the rugged terrain two bands of pirates have come in search of the
  ever elusive lolrus, even Mr Coconut has come in search! Defeat ye enemy for a
  chance to glimpse the beast!
date: '2007-09-21'
categories:
  - Battlefield 1942
  - Anniversary
tags:
  - Conquest
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 1024
  source: anniversary
  controlpoints:
    - name: Skull Clan Tower
      id: SkullClan_Tower
      position:
        x: '672.37'
        'y': '173.06'
        z: '526.06'
    - name: Pegleg Tower
      id: Pegleg_Tower
      position:
        x: '408.08'
        'y': '173.28'
        z: '525.05'
  created: '2007-09-21'
  creator: Cap'n Pugwash
---

