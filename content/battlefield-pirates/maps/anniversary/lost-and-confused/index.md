---
title: Lost And Confused
author: Muad'Dib
description: >-
  The lolrus has hidden some treasures around the maze, but beware danger lurks
  around every corner. Don't get yourself lost or dizzy, fighting off the other
  pirates! You might even see the lolrus!
date: '2007-09-21'
categories:
  - Battlefield 1942
  - Anniversary
tags:
  - Conquest
  - CTF
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 1024
  source: anniversary
  controlpoints:
    - name: Peggies
      id: peggies
      position:
        x: '130.00'
        'y': '101.69'
        z: '600.00'
    - name: Skullc
      id: skullc
      position:
        x: '825.00'
        'y': '94.06'
        z: '250.00'
    - name: North
      id: North
      position:
        x: '518.03'
        'y': '88.34'
        z: '859.34'
    - name: South
      id: South
      position:
        x: '409.63'
        'y': '88.96'
        z: '153.66'
    - name: Middle
      id: Middle
      position:
        x: '512.00'
        'y': '88.53'
        z: '512.00'
  created: '2007-09-21'
  creator: Muad'Dib
---

