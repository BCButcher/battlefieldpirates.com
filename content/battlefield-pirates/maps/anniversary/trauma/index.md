---
title: Trauma
author: Cap'n Pugwash
description: >-
  Assault the church by all means necessary to win here! Sometimes the most
  obvious way in isn't always the best...
date: '2007-09-21'
categories:
  - Battlefield 1942
  - Anniversary
tags:
  - Conquest
labels: []
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 1024
  source: anniversary
  controlpoints:
    - name: Axis Base
      id: AxisBase
      position:
        x: '359.00'
        'y': '86.81'
        z: '757.42'
    - name: Allied Base
      id: AlliedBase
      position:
        x: '359.00'
        'y': '86.95'
        z: '521.36'
    - name: Church
      id: Church
      position:
        x: '358.74'
        'y': '80.96'
        z: '633.87'
  created: '2007-09-21'
  creator: Cap'n Pugwash
---

