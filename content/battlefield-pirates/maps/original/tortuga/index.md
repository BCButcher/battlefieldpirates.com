---
title: Tortuga
author: Anymousity
description: >-
  It seems that a 'retired' PegLeg has become governor of Tortuga. Now he and
  his followers are living the good life. Unfortunately, the SkullClan thinks
  it's their turn and wants in. This battle will determine who gets control of
  the city. That is, if they don't destroy it first..........All flags are
  capturable, even the ship. Be sure to explore all the nooks and crannies
  because the main route may not always be the best.
date: '2004-07-13'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - BFP1 Beta 3.1
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 1024
  source: original
  controlpoints:
    - name: Skull Clan Galleon
      id: SkullClan_Galleon
      position:
        x: '325.90'
        'y': '88.39'
        z: '699.73'
    - name: Graveyard
      id: Graveyard
      position:
        x: '314.61'
        'y': '83.42'
        z: '820.46'
    - name: Gallows
      id: Gallows
      position:
        x: '361.77'
        'y': '81.43'
        z: '852.14'
    - name: Well
      id: Well
      position:
        x: '214.52'
        'y': '82.31'
        z: '892.69'
    - name: Mayors Mansion
      id: Mayors_Mansion
      position:
        x: '147.92'
        'y': '107.99'
        z: '849.37'
  created: '2004-07-13'
  creator: Anymousity
---

