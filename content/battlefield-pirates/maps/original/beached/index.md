---
title: Beached
author: ''
description: >-
  Stranded on this new found world, the peglegs search out in order to establish
  a camp. What do ya know? There's the Skullclan! Looks like they came here to
  greet'em...
date: '2005-11-01'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - BFP1 Final 1.0
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 1024
  source: original
  controlpoints:
    - name: Blue Spawn
      id: Blue_Spawn
      position:
        x: '340.55'
        'y': '63.97'
        z: '597.89'
    - name: Forbidden Cove
      id: Forbidden_Cove
      position:
        x: '424.80'
        'y': '73.43'
        z: '582.68'
    - name: Buried Fortification
      id: Buried_Fortification
      position:
        x: '473.72'
        'y': '76.35'
        z: '714.42'
    - name: The Forest
      id: The_Forest
      position:
        x: '407.14'
        'y': '58.81'
        z: '670.97'
  created: '2005-11-01'
---

