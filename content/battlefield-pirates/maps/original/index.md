---
title: Battlefield Pirates Original
date: 2005-11-22
description: >-
  Battlefield Pirates is a full conversion mod of Battlefield 1942. The mod was
  first released in the summer of 2003, initially a solo project from creator
  Guy Smiley (Laurence Brown). BFP v1.0 was released on August 23rd, 2005,
  marking the final release of Battlefield Pirates for Battlefield 1942.


  The mod featured two playable pirate crews, the red pirates versus the blue
  pirates. This evolved to become the undead Skull Clan against the human
  Peglegs in version .3 and beyond. This version also introduced numerous new
  weapons to the game such as throwing knives and the musketoon.


  Battlefield Pirates features a unique combat system that balances ranged
  musket fire with swordplay. Flintlock weaponry demands long reload times,
  forcing skilled pirates to hone their skills with a sword to stay alive. This
  game was cherished by skilled players for it's fluid gameplay and balanced
  weapons.


  The game also featured an armada of fully operational ships ranging from small
  dinghy rowboats to massive galleons. Players could raise and lower rows of
  cannons, board and steal enemy vessels and even pilot hot air balloons.


  Brace yourself for a fun-filled adventure on the high seas!
categories:
  - Pirates
type: source
layout: layouts/source.njk
origin: >-
  https://web.archive.org/web/20090225072254/http://bfpirates.com:80/wiki/index.php/BFP
---
