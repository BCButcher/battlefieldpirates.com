---
title: Peninsula Of Pain
author: ''
description: ''
date: '2004-07-13'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - BFP1 Beta 3.1
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: original
  controlpoints:
    - name: Southern Point
      id: Southern_Point
      position:
        x: '903.87'
        'y': '60.10'
        z: '727.78'
    - name: Tower
      id: Tower
      position:
        x: '1055.05'
        'y': '104.89'
        z: '983.45'
    - name: Center Isle
      id: Center_Isle
      position:
        x: '1103.65'
        'y': '49.12'
        z: '1103.89'
    - name: Pirates Peak
      id: Pirates_Peak
      position:
        x: '1245.44'
        'y': '87.69'
        z: '1424.65'
    - name: Buried Treasure
      id: Buried_Treasure
      position:
        x: '1038.84'
        'y': '43.09'
        z: '848.14'
    - name: Skull Clan Camp
      id: SkullClan_Camp
      position:
        x: '619.47'
        'y': '42.87'
        z: '611.17'
    - name: Peg Leg Camp
      id: PegLeg_Camp
      position:
        x: '1204.47'
        'y': '42.87'
        z: '610.28'
  created: '2004-07-13'
---

