---
title: Azure Seas
author: ''
description: >-
  Recent volcanic activity has brought silver to the surface. The silver nuggets
  found awash the beaches have made these isles highly desired.
date: '2005-11-01'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - BFP1 Final 1.0
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: original
  controlpoints:
    - name: Skull Clan Hideout
      id: SkullClan_Hideout
      position:
        x: '1006.54'
        'y': '41.51'
        z: '1291.45'
    - name: Peg Leg Camp
      id: PegLeg_Camp
      position:
        x: '1003.36'
        'y': '41.47'
        z: '777.34'
    - name: Bullion Bay
      id: Bullion_Bay
      position:
        x: '1225.55'
        'y': '73.63'
        z: '1047.02'
    - name: Smugglers Cove
      id: Smugglers_Cove
      position:
        x: '839.52'
        'y': '41.03'
        z: '1037.86'
    - name: Beach
      id: Beach
      position:
        x: '1086.46'
        'y': '41.55'
        z: '976.60'
  created: '2005-11-01'
---

