---
title: Shark Haven
author: ''
description: >-
  Arh Mateys! Ye be ichin' for some fightin'? Well here ye'll get yer chance!
  These here two captured Spanish forts at Sharks Haven be prime land for a
  fight! Ye enemies will be a waitin' for you on the other side so give 'em the
  steel and send them down to Davey Jones!
date: '2004-07-13'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - CTF
labels:
  - BFP1 Beta 3.1
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
  size: 1024
  source: original
  controlpoints:
    - name: Red Pirate Fort
      id: RedPirateFort
      position:
        x: '429.38'
        'y': '81.46'
        z: '596.24'
    - name: Blue Pirate Fort
      id: BluePirateFort
      position:
        x: '634.67'
        'y': '82.44'
        z: '592.24'
    - name: Blue Harbor Master
      id: BlueHarborMaster
      position:
        x: '737.12'
        'y': '44.93'
        z: '519.81'
    - name: Red Harbor Master
      id: RedHarborMaster
      position:
        x: '301.80'
        'y': '44.52'
        z: '530.78'
    - name: Channel Fort
      id: Channel_Fort
      position:
        x: '537.94'
        'y': '43.11'
        z: '594.10'
  created: '2004-07-13'
---

