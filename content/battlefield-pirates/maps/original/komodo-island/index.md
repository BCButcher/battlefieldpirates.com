---
title: Komodo Island
author: ''
description: >-
  For many years the Redbeards feasted heartily on their Komodo dragon
  delicacies found only on Komodo Island. Now the Bluebeards have come to stake
  their claim and drive the Redbeards off the island once and for all
date: '2004-02-11'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - BFP1 Alpha 0.23
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: original
  controlpoints:
    - name: Village
      id: Village
      position:
        x: '1113.83'
        'y': '56.72'
        z: '1193.74'
    - name: Blue Island
      id: Blue_Island
      position:
        x: '968.41'
        'y': '43.50'
        z: '819.15'
    - name: Komodo Dungeon
      id: Komodo_Dungeon
      position:
        x: '1112.30'
        'y': '47.79'
        z: '1050.01'
    - name: Camp
      id: Camp
      position:
        x: '1154.39'
        'y': '40.54'
        z: '892.45'
    - name: Forest Hill
      id: ForestHill
      position:
        x: '1259.65'
        'y': '59.70'
        z: '1060.48'
    - name: Jungle
      id: Jungle
      position:
        x: '1243.97'
        'y': '45.58'
        z: '1211.72'
  created: '2004-02-11'
---

