---
title: Black Sand Buccaneers
author: ''
description: >-
  This island is very popular, both for it's luxurious black-sand beaches, and
  the gold rumored to be buried at the center. As usual, it seems this island
  ain't big enough for the two of you..........Capturing the center is the only
  way to cause ticket bleed, but you can't spawn there, so you'll have to send
  reinforcements if you want to hold it. Another good stategy would be to keep
  your enemy from getting there in the first place. Harass him for all you're
  worth at his home camp if you wanna own this island! But be warned: He's gonna
  try to do the same to you!
date: '2004-07-13'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - BFP1 Beta 3.1
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 1024
  source: original
  controlpoints:
    - name: Skull Camp
      id: Skull_Camp
      position:
        x: '504.76'
        'y': '49.31'
        z: '747.55'
    - name: Peg Leg Camp
      id: PegLeg_Camp
      position:
        x: '568.74'
        'y': '49.52'
        z: '248.25'
    - name: Island
      id: Island
      position:
        x: '524.39'
        'y': '48.21'
        z: '507.61'
  created: '2004-07-13'
---

