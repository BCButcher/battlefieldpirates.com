---
title: Two Forts
author: ''
description: >-
  An all-out Galleon slug fest! Just what you want most. Capture both forts to
  cause ticket drain, use the Fortress Cannons to send your enemy to the bottom
  of the sea, or just rain shot down on his decks!..........In this map, a new
  Galleon will respawn almost as fast as the old one sinks, so keep those
  cannons blazing.
date: '2004-07-13'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - BFP1 Beta 3.1
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: original
  controlpoints:
    - name: East Fort
      id: East_Fort
      position:
        x: '1101.00'
        'y': '82.99'
        z: '1024.00'
    - name: West Fort
      id: West_Fort
      position:
        x: '947.00'
        'y': '82.99'
        z: '1024.00'
  created: '2004-07-13'
---

