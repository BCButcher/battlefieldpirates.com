---
title: Low Tide
author: ''
description: ''
date: '2003-06-27'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - CTF
  - TDM
labels:
  - BFP1 Alpha 0.1
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - CTF
    - TDM
  size: 1024
  source: original
  controlpoints:
    - name: Wreck Control 02
      id: wreck_control_02
      position:
        x: '469.000000'
        'y': '42.299999'
        z: '472.000000'
    - name: Wreck Control 01
      id: wreck_control_01
      position:
        x: '155.634293'
        'y': '41.172100'
        z: '160.283600'
    - name: Wreck Control 03
      id: wreck_control_03
      position:
        x: '719.797485'
        'y': '42.130798'
        z: '430.104004'
    - name: Wreck Control 04
      id: wreck_control_04
      position:
        x: '863.766785'
        'y': '41.418999'
        z: '856.498718'
    - name: Wreck Control 05
      id: wreck_control_05
      position:
        x: '281.194885'
        'y': '42.044701'
        z: '533.203186'
    - name: Red Base Cpoint
      id: RedBase_Cpoint
      position:
        x: '261.959015'
        'y': '43.054401'
        z: '832.700806'
    - name: Blue Base Cpoint
      id: BlueBase_Cpoint
      position:
        x: '692.234375'
        'y': '43.316101'
        z: '148.478302'
  created: '2003-06-27'
---

