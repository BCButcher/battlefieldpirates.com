---
title: Domes Of Doom
author: ''
description: ''
date: '2004-02-11'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - BFP1 Alpha 0.23
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 2048
  source: original
  controlpoints:
    - name: Skull Clan Mountain
      id: SkullClan_Mountain
      position:
        x: '1406.49'
        'y': '43.29'
        z: '1428.20'
    - name: Peg Leg Mountain
      id: PegLeg_Mountain
      position:
        x: '655.33'
        'y': '43.29'
        z: '604.05'
    - name: South Volcano
      id: South_Volcano
      position:
        x: '856.23'
        'y': '102.00'
        z: '707.95'
    - name: North Volcano
      id: North_Volcano
      position:
        x: '1189.75'
        'y': '101.69'
        z: '1342.72'
    - name: Center Volcano 1
      id: Center_Volcano_1
      position:
        x: '966.41'
        'y': '170.62'
        z: '943.65'
    - name: Center Volcano 2
      id: Center_Volcano_2
      position:
        x: '1082.03'
        'y': '170.49'
        z: '1054.45'
    - name: Center Volcano 3
      id: Center_Volcano_3
      position:
        x: '945.06'
        'y': '170.71'
        z: '1088.65'
  created: '2004-02-11'
---

