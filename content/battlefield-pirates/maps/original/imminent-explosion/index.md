---
title: Imminent Explosion
author: ''
description: ''
date: '2005-11-01'
categories:
  - Battlefield 1942
  - Original
tags:
  - CTF
labels:
  - BFP1 Final 1.0
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - CTF
  size: 1024
  source: original
  controlpoints:
    - name: Skull Clan Flag
      id: SkullClan_Flag
      position:
        x: '702.00'
        'y': '53.84'
        z: '492.00'
    - name: Peg Legs Flag
      id: PegLegs_Flag
      position:
        x: '534.00'
        'y': '55.30'
        z: '506.00'
  created: '2005-11-01'
---

