---
title: Prison Isle
author: ''
description: >-
  You and your enemies have been thrown into the Prison Isle work camp. Together
  you have managed to dig several escape tunnels and now is your chance, but now
  that you've escaped, all bets are off and it's pirate against
  pirate..........Destroy all the enemy's escape tunnel exits with powderkegs
  and capture the guard house flag to throw your enemy back into the prison,
  thereby allowing you to destroy them at your leisure. If stuck in the prison,
  it's possible to blow the prison gates to escape again. If you take too long,
  escape tunnels will begin reappearing on the map.
date: '2004-07-13'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
labels:
  - BFP1 Beta 3.1
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
  size: 1024
  source: original
  controlpoints:
    - name: Axis Base
      id: AxisBase
      position:
        x: '530.40'
        'y': '88.95'
        z: '575.28'
    - name: Allied Base
      id: AlliedBase
      position:
        x: '499.15'
        'y': '88.96'
        z: '565.29'
    - name: Guard House
      id: Guard_House
      position:
        x: '512.14'
        'y': '100.79'
        z: '413.16'
  created: '2004-07-13'
---

