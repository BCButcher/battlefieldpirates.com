---
title: Scurvy Cove
author: ''
description: ''
date: '2003-06-27'
categories:
  - Battlefield 1942
  - Original
tags:
  - Conquest
  - Coop
  - CTF
  - TDM
labels:
  - BFP1 Alpha 0.11
type: map
layout: layouts/map.njk
content: ''
map:
  gametypes:
    - Conquest
    - Coop
    - CTF
    - TDM
  size: 2048
  source: original
  controlpoints:
    - name: Blue Base Cpoint
      id: BlueBase_Cpoint
      position:
        x: '108'
        'y': '50.172'
        z: '100'
    - name: Blue Base Cpoint 2
      id: BlueBase_Cpoint2
      position:
        x: '633'
        'y': '66.463'
        z: '817.408'
    - name: Red Base Cpoint
      id: RedBase_Cpoint
      position:
        x: '1940'
        'y': '50.172'
        z: '1948'
    - name: Red Base Cpoint 2
      id: RedBase_Cpoint2
      position:
        x: '1411.707'
        'y': '58'
        z: '1189.037'
  created: '2003-06-27'
---

