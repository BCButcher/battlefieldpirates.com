---
title: Pirates 1942 Classes
categories:
  - Battlefield 1942
type: mod
layout: layouts/details.njk
preface:
  - name: Peglegs
    icon: pirate-2.svg
    description: "The human Peglegs, formerly the blue pirates."
  - name: Skullclan
    icon: pirate-1.svg
    description: "The undead Skull Clan, formerly the red pirates."
details:
  - name: Kits
    icon: pirate-3.svg
    items:
      - name: Huntsmen
        image: img/game/bfp1/kits/huntsmen.png
        points:
          - name: Cutlass
            image: img/game/bfp1/weapons/handheld/icons/cutlass.png
            link: ../weapons/#cutlass
          - name: Pistol
            image: img/game/bfp1/weapons/handheld/icons/pistol.png
            link: ../weapons/#pistol
          - name: Blunderbuss
            image: img/game/bfp1/weapons/handheld/icons/blunderbuss.png
            link: ../weapons/#blunderbuss
          - name: Spyglass
            image: img/game/bfp1/weapons/utility/icons/spyglass.png
            link: ../weapons/#spyglass
        description: "Ah! The thrill of the blunderbuss, the pirate shotgun. This class is made for the close range and melee. It starts with the blunderbuss, which shoots a spray of shots that have a great effect up close, and suffer even greater at distance. Aim isn't as important as with the other weapons because the spread of shot takes care of that. It is the perfect weapon for clearing ship decks and blasting foes up close. This class also has throwing knives/axes. These allow for a quick follow up to a blunder blast or a stealthy kill. They are also effective against swordsmen. The knives/axes are also only for close range though as they arc down quickly after released. This class also has a telescope, which can be used to spy and to call in artillery strikes. This feature is seldom used in BFP, but will hopefully see an increase. In addition, of course this class has the cutlass as its melee weapon. The Huntsman is melee and close combat oriented, being the best of the three main classes at close combat and the worst at long distance."
        addendum:
          - 'Don''t "blundersnipe", if the target is over several yards away don''t fire, this is called blundersniping and since the blunderbuss is ineffective at medium-long range it does very little damage, and will only alert the enemy to your presence.'
          - "The blunder also zooms, but don't use if you won't need it."
          - "Try to wound the opponent with your blunderbuss (if you can't kill him outright), then move to knives/axes, then to cutlass. The huntsman is perfect for whittling down an opponent before a swordfight."
      - name: Raiders
        image: img/game/bfp1/kits/raiders.png
        points:
          - name: Cutlass
            image: img/game/bfp1/weapons/handheld/icons/cutlass.png
            link: ../weapons/#cutlass
          - name: Pistol
            image: img/game/bfp1/weapons/handheld/icons/pistol.png
            link: ../weapons/#pistol
          - name: Musket
            image: img/game/bfp1/weapons/handheld/icons/musket.png
            link: ../weapons/#musket
          - name: Grenade
            image: img/game/bfp1/weapons/handheld/icons/grenade.png
            link: ../weapons/#grenade
        description: "The BFP sniper, as this class is known, is the best at ranged combat. This class has a musket, which can zoom substantially and has great range. It is the default and probably most used class overall. The musket is very powerful and in the hands of a good shot can destroy several enemies. The Raider also has a pistol. This pistol is the quickest drawing weapon in the game (think old West). If you don't kill them with your musket a quick pistol shot will work. It has great range as well and can be used by skilled players to pistol snipe. This class also has 1 grenade (You can't get another one unless you pick up one that hasn't already been used from a corpse) which can come in useful for reasons not needing to be explained. Moreover, of course the Raider has a cutlass. This class is the best of the main 3 at distance and the weakest at close range."
        addendum:
          - "If your musket doesn't kill outright, immediately pull pistol and fire."
          - "You can pistol snipe enemies that are standing still by first shooting with your musket, and if they still live, not shifting aim, but rather using pistol from the exact same position."
          - "Don't forget the nade, it is great for clearing rooms."
          - "For old BFP players forget the sticky pistol bug, it is gone, now you only have 1 shot per pistol reload, but the cutlass can be pulled immediately after firing."
      - name: Carpenter
        image: img/game/bfp1/kits/cannoneers.png
        points:
          - name: Cutlass
            image: img/game/bfp1/weapons/handheld/icons/cutlass.png
            link: ../weapons/#cutlass
          - name: Throwing knives (Peglegs)
            image: img/game/bfp1/weapons/handheld/icons/throwingknife.png
            link: ../weapons/#throwing-knife
          - name: Stone Axes (SkullClan)
            image: img/game/bfp1/weapons/handheld/icons/stoneaxe.png
            link: ../weapons/#stone-axe
          - name: Musketoon
            image: img/game/bfp1/weapons/handheld/icons/musket.png
            link: ../weapons/#musketoon
          - name: Powderkegs
            image: img/game/bfp1/weapons/utility/icons/powderkeg.png
            link: ../weapons/#powder-keg
          - name: Wrench
            image: img/game/bfp1/weapons/utility/icons/wrench.png
            link: ../weapons/#wrench
        description: "This is the BFP engineer class and one well suited to combat. The Carpenter has a musketoon, which is a shorter musket unable to zoom. It as such cannot snipe very well, but packs a punch at close and medium range. Like the Huntsman, this class has throwing knives/axes for a close range assault. The big element of this class is the kegs; Carpenters have 2 refillable kegs that have long fuses. These can blow up walls, cannons, towers, and if lucky enemies. As the engineer, class can repair with their handy wrench. Ships, other vehicles, towers, and cannons can be repaired. Top it off with the cutlass. This class is very well balanced, and is the second best of the main classes in both close combat and long range combat. The intangibles of the kegs and repair ability make this a favorite of many BFP veterans."
        addendum:
          - "Use kegs to take out cannons and towers so the enemy won't catch you off guard later."
          - "Don't forget the repair ability, in some ship battles this can mean sink or swim."
          - "While not a musket, the musketoon can be a decent sniper weapon in the right hands."
          - "Ah kegs, 2 will blow up towers, walls and gates will blow as well with a few extra."
      - name: Cannoneers
        image: img/game/bfp1/kits/cannoneers.png
        points:
          - name: Cutlass
            image: img/game/bfp1/weapons/handheld/icons/cutlass.png
            link: ../weapons/#cutlass
          - name: Ducksfoot Pistol
            image: img/game/bfp1/weapons/handheld/icons/ducksfoot.png
            link: ../weapons/#ducksfoot-pistol
          - name: Mobile 8-pounder Cannon
            image: img/game/bfp1/weapons/utility/icons/8pdr.png
            link: ../weapons/#8-pounder
        description: "This class is the heavy hitter of BFP. Instead of a regular gun, the cannoneer has a deployable 8lb cannon. This cannon is quite accurate when used properly and is great for defending and in some cases attacking. The cannon can be wheeled around to your advantage, and strategically placed for the team advantage. The only gun this class carries is the ducksfoot pistol. This gun is a close range weapon that spews 5 shots at an enemy. In this way it resembles the blunderbuss in the short game, though it only has 2 reloads to limit its usefulness. The cutlass rounds out this class. This is a great support class on certain maps"
        addendum:
          - "You can switch from driving your cannon around to firing by changing positions quickly (hitting 1 or 2). This can be used to blast enemies that approach when you are moving your cannon."
          - "The ducksfoot is excellent for melee, blast a foe, and then pull your sword and swing."
          - "Place your 8lber in a high traffic area for maximum effect."
      - name: Drunkards
        points:
          - name: Tankard
            image: img/game/bfp1/weapons/handheld/icons/tankard.png
            link: ../weapons/#tankard
        image: img/game/bfp1/kits/drunkards.png
        description: "This class is more of a joke and fun class than a regular class. The only weapon is... a mug. It works like a cutlass, though supposedly has a touch more range. Skilled cutlass users can use this class to great effect, though it isn't recommended for most players. A joke class, it is, though it can work for certain players in certain maps."
        addendum:
          - "The best element of the drunkard is stealth. With no weaponry, you can't make noise and so might find it easier to sneak into places. *On certain maps (like Dead Calm), you can only be a certain class, and this class might have different weaponry than on regular maps."
---
