const util = require("util");
const resolvePath = require("object-resolve-path");

const sortAphabetical = (x, y) => {
  if (x.toLowerCase() !== y.toLowerCase()) {
    x = x.toLowerCase();
    y = y.toLowerCase();
  }
  return x > y ? 1 : x < y ? -1 : 0;
};

const getKeys = (dataSource, key) => {
  let keySet = new Set();

  dataSource.forEach((item) => {
    if (!item.hidden) {
      var keys = resolvePath(item, String(key));
      if (keys) {
        if (typeof keys === "string") {
          keys = [keys];
        }
        if (typeof keys === "number") {
          keys = [String(keys)];
        }
        for (const value of keys) {
          if (!value.startsWith("_")) {
            keySet.add(value);
          }
        }
      }
    }
  });
  return [...keySet].sort(sortAphabetical);
};

const getItemsByKey = (dataSource, key, value) => {
  var result = dataSource.filter((item) => {
    if (item.hidden) {
      return false;
    }
    var keys = resolvePath(item, String(key));
    if (keys) {
      var match = false;
      if (typeof keys === "string") {
        keys = [keys];
      }
      if (typeof keys === "number") {
        keys = [String(keys)];
      }

      keys.forEach((keyValue) => {
        if (keyValue == value) {
          match = true;
        }
      });
      return match;
    }
    return false;
  });

  result = result.sort((a, b) => {
    return b.date - a.date;
  });

  return result;
};

/**
 * Get Collection as object with keys and iterable items
 * @param {Collection} collection
 * @param {string} key
 * @returns Collection as key-value pairs
 */
const itemsByKeys = (collection, key) => {
  var keySet = {};
  var newSet = new Set();
  var dataSource = collection.getAll();
  // Collections store useful data in collections.data
  key = "data." + String(key);

  keySet = getKeys(dataSource, key);

  keySet.forEach((value) => {
    newSet[value] = getItemsByKey(dataSource, key, value);
  });

  return { ...newSet };
};

exports.byCategories = (collection) => {
  return itemsByKeys(collection, "categories");
};

exports.byLabels = (collection) => {
  return itemsByKeys(collection, "labels");
};
