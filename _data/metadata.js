module.exports = {
  title: "Battlefield Pirates",
  domain: "BattlefieldPirates.com",
  url: "https://battlefieldpirates.com",
  image: "/social.webp",
  language: "en",
  language_bcp47: "en-GB",
  language_icu: "en_GB",
  tags: "battlefield pirates, battlefield 1942, battlefield 2, pirates",
  description:
    "Battlefield Pirates is a full conversion mod of Battlefield 1942 and Battlefield 2.",
  footer:
    "[BattlefieldPirates.com](https://gitlab.com/BCButcher/BattlefieldPirates) by [Butcher](https://gitlab.com/BCButcher), based on the original [BFPirates.com](https://web.archive.org/web/20080723032344/http://www.bfpirates.com/). All content copyright by their respective, original authors.",
  author: {
    name: "Butcher",
    url: "https://gitlab.com/BCButcher",
  },
  type: "WebPage",
};
