function handleSearchCount() {
  if (!document.querySelector("[type='search']")) {
    return;
  }
  let filteredItems = document.querySelectorAll(
    "masonry brick:not([aria-hidden='true'])",
  );
  if (filteredItems.length > 0) {
    document.querySelector(
      "[type='search']",
    ).placeholder = `Search ${filteredItems.length} items`;
  } else {
    document.querySelector("[type='search']").placeholder = `Search`;
  }
}

function handleFiltering() {
  let categoriesButtonGroup = document.querySelector(
    "[data-filter-group='categories']",
  );
  let tagsButtonGroup = document.querySelector("[data-filter-group='tags']");

  const clearActive = function (buttonGroup) {
    let buttons = buttonGroup.querySelectorAll("button");
    for (let i = 0; i < buttons.length; i++) {
      buttons[i].removeAttribute("active");
    }
  };
  const setActive = function (buttonGroup, match) {
    buttonGroup
      .querySelector(`[data-filter="${match}"]`)
      .setAttribute("active", "");
  };

  if (filters.hasOwnProperty("categories")) {
    clearActive(categoriesButtonGroup);
    if (filters.categories !== "") {
      setActive(categoriesButtonGroup, filters.categories);
    }
    if (filters.categories === "") {
      setActive(categoriesButtonGroup, "");
    }
  }
  if (filters.hasOwnProperty("tags")) {
    clearActive(tagsButtonGroup);
    if (filters.tags !== "") {
      setActive(tagsButtonGroup, filters.tags);
    }
    if (filters.tags === "") {
      setActive(tagsButtonGroup, "");
    }
  }
  if (Object.keys(filters).length < 1) {
    clearActive(categoriesButtonGroup);
    clearActive(tagsButtonGroup);
    document.querySelector("[type='search']").value = "";
  }
  handleSearchCount();
}

function filter() {
  shuffleInstance.filter((element) => {
    var isMatched = true;
    for (const [key, value] of Object.entries(filters)) {
      if (key == "categories" && value !== "") {
        if (!JSON.parse(element.dataset.categories).includes(value)) {
          isMatched = false;
        }
      }
      if (key == "tags" && value !== "") {
        if (!JSON.parse(element.dataset.tags).includes(value)) {
          isMatched = false;
        }
      }
      if (key == "search" && value !== "") {
        if (!element.dataset.title.match(filters.search)) {
          isMatched = false;
        }
      }
    }
    return isMatched;
  });
}

if (document.querySelector("[type='search']")) {
  document
    .querySelector("[type='search']")
    .addEventListener("input", function (event) {
      const inputText = event.target.value;
      debounce(function (inputText) {
        filters.search = new RegExp(inputText, "gi");
        filter();
        handleFiltering();
      })(inputText);
    });
}

const Shuffle = window.Shuffle;
const shuffleInstance = new Shuffle(document.querySelector("masonry"), {
  itemSelector: "brick",
});
var filters = {};
handleSearchCount();

if (document.querySelector(".filters")) {
  document.querySelectorAll(".filters button").forEach((button) =>
    button.addEventListener("click", function (event) {
      let buttonGroup = event.target.parentNode;
      let filterGroup = buttonGroup.getAttribute("data-filter-group");
      filters[filterGroup] = event.target.getAttribute("data-filter");
      if (event.target.getAttribute("data-filter") === "") {
        filters = {};
        shuffleInstance.filter();
      }
      filter();
      handleFiltering();
    }),
  );
}
