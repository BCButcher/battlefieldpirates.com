const { execSync } = require("child_process");
const fs = require("fs");
const util = require("util");
const path = require("path");
const chalk = require("chalk-cjs");
const lodash = require("lodash");
const dayjs = require("dayjs");
const { DateTime } = require("luxon");
const yaml = require("js-yaml");
const njkFilters = require("nunjucks/src/filters");
const striptags = require("striptags");
const prettier = require("prettier");
const LinkedData = require("./src/LinkedData.js");

const MarkdownOptions = {
  html: true,
  breaks: false,
  linkify: true,
  typographer: true,
};
const markdownIt = require("markdown-it");
const markdownItAnchor = require("markdown-it-anchor");
const markdownItEleventyImg = require("markdown-it-eleventy-img");
const markdownItFootnote = require("markdown-it-footnote");

const pluginRss = require("@11ty/eleventy-plugin-rss");
const pluginBundle = require("@11ty/eleventy-plugin-bundle");
const pluginNavigation = require("@11ty/eleventy-navigation");
const { EleventyHtmlBasePlugin } = require("@11ty/eleventy");
const { EleventyRenderPlugin } = require("@11ty/eleventy");
const pluginWebc = require("@11ty/eleventy-plugin-webc");
const readingTime = require("eleventy-plugin-reading-time");
const safeLinks = require("@sardine/eleventy-plugin-external-links");

const FrontMatter = require("gray-matter");
const CleanCSS = require("clean-css");

const metadata = require("./_data/metadata");

const matter = (path) => {
  if (typeof path == "undefined" || path == "") {
    return false;
  }
  const file = fs.readFileSync(path, "utf8");
  const contents = file.toString();
  const matter = FrontMatter(contents);
  return matter.data;
};

module.exports = function (eleventyConfig) {
  eleventyConfig.addFilter("getContext", function () {
    return this.ctx;
  });
  eleventyConfig.addNunjucksGlobal("getContext", function () {
    return this.ctx;
  });
  eleventyConfig.addNunjucksGlobal("fileExists", function (path) {
    if (!path || !fs.existsSync(path)) {
      return false;
    }
    return true;
  });

  // Copy the contents of the `public` folder to the output folder
  eleventyConfig.addPassthroughCopy({
    "./public/": "/",
  });
  eleventyConfig.addPassthroughCopy("content/**/*.{svg,webp,png,jpeg,jpg}");

  eleventyConfig.addDataExtension("yaml", (contents) => yaml.load(contents));

  // Watch content images for the image pipeline.
  eleventyConfig.addWatchTarget("content/**/*.{svg,webp,png,jpeg,jpg}");

  // App plugins
  eleventyConfig.addPlugin(require("./src/11ty.drafts.js"));
  // eleventyConfig.addPlugin(require("./src/11ty.images.js"));
  eleventyConfig.addPlugin(require("./src/11ty.highlighter.js"), {
    // theme: require("./local_modules/shiki-themes/nord.json"),
    theme: "github-light",
  });

  // Official plugins
  eleventyConfig.addPlugin(pluginRss);
  eleventyConfig.addPlugin(pluginNavigation);
  eleventyConfig.addPlugin(EleventyHtmlBasePlugin);
  eleventyConfig.addPlugin(EleventyRenderPlugin);
  eleventyConfig.addPlugin(pluginWebc, {
    components: "_includes/components/**/*.webc",
  });
  eleventyConfig.addPlugin(pluginBundle);

  // Third-party plugins
  eleventyConfig.addPlugin(readingTime);
  eleventyConfig.addPlugin(safeLinks);

  eleventyConfig.addNunjucksGlobal("FrontMatter", (page) => {
    return matter(page.inputPath);
  });

  // Filters
  eleventyConfig.addFilter("array", (input) => Array.isArray(input));
  eleventyConfig.addFilter("readableDate", (dateObj, format, zone) => {
    if (dateObj instanceof Date) {
      return DateTime.fromJSDate(dateObj, { zone: "utc" }).toFormat(
        format || "dd LLLL yyyy",
      );
    } else {
      return DateTime.fromISO(dateObj, {
        zone: zone || "utc",
      }).toFormat(format || "dd LLLL yyyy");
    }
  });
  eleventyConfig.addFilter("htmlDateString", (dateObj) => {
    return DateTime.fromJSDate(dateObj, { zone: "utc" })
      .setLocale(metadata.language)
      .toLocaleString(DateTime.DATE_FULL);
  });
  eleventyConfig.addFilter("iso8601", (dateObj) => {
    if (dateObj instanceof Date) {
      return DateTime.fromJSDate(dateObj, { zone: "utc" }).toISO();
    } else {
      return DateTime.fromISO(dateObj, { zone: "utc" }).toISO();
    }
  });
  eleventyConfig.addFilter("include", (arr, path, value) => {
    value = lodash.deburr(value).toLowerCase();
    return arr.filter((item) => {
      let pathValue = lodash.get(item, path);
      pathValue = lodash.deburr(pathValue).toLowerCase();
      return pathValue.includes(value);
    });
  });
  eleventyConfig.addFilter("removeself", function (collection, self) {
    if (!self) {
      return collection;
    }
    return collection.filter((item) => item.url !== self.url);
  });
  eleventyConfig.addFilter("markdown", (content) => {
    const md = new markdownIt(MarkdownOptions);
    return md.render(content);
  });
  eleventyConfig.addFilter("jsonstringify", (content) => {
    return JSON.stringify(content);
  });

  // Get the first `n` elements of a collection.
  eleventyConfig.addFilter("head", (array, n) => {
    if (!Array.isArray(array) || array.length === 0) {
      return [];
    }
    if (n < 0) {
      return array.slice(n);
    }

    return array.slice(0, n);
  });
  // Return the smallest number argument
  eleventyConfig.addFilter("min", (...numbers) => {
    return Math.min.apply(null, numbers);
  });
  // Return all the categories used in a collection
  eleventyConfig.addFilter("getAllCategories", (collection) => {
    let dataSet = new Set();
    for (let item of collection) {
      (item.data.categories || []).forEach((item) => dataSet.add(item));
    }
    return Array.from(dataSet);
  });
  // Return all the tags used in a collection
  eleventyConfig.addFilter("getAllTags", (collection) => {
    let dataSet = new Set();
    for (let item of collection) {
      (item.data.tags || []).forEach((item) => dataSet.add(item));
    }
    return Array.from(dataSet);
  });
  // Return all the labels used in a collection
  eleventyConfig.addFilter("getAllLabels", (collection) => {
    let dataSet = new Set();
    for (let item of collection) {
      (item.data.labels || []).forEach((item) => dataSet.add(item));
    }
    return Array.from(dataSet);
  });
  eleventyConfig.addFilter("filterTagList", function filterTagList(tags) {
    return (tags || []).filter(
      (tag) => ["all", "nav", "post", "posts", "writing"].indexOf(tag) === -1,
    );
  });

  eleventyConfig.addCollection("everything", (collectionApi) => {
    const macroImport = `{% import "macros/macros.njk" as macro with context %}`;
    let collection = collectionApi.getFilteredByGlob("content/**/*.njk");
    collection.forEach((item) => {
      item.template.frontMatter.content = `${macroImport}\n${item.template.frontMatter.content}`;
    });
    return collection;
  });
  eleventyConfig.addCollection("pages", function (collection) {
    return collection.getFilteredByGlob("**/*.md");
  });
  eleventyConfig.addCollection(
    "categories",
    require("./src/collections/11ty.contentByKey").byCategories,
  );
  eleventyConfig.addCollection(
    "labels",
    require("./src/collections/11ty.contentByKey").byLabels,
  );
  eleventyConfig.addCollection("tags", (collection) => {
    const dataSet = new Set();
    collection.getAll().forEach((item) => {
      if (!item.data.tags) {
        return;
      }
      item.data.tags.forEach((tag) => dataSet.add(tag));
    });
    return dataSet;
  });

  eleventyConfig.addFilter("groupByYear", (collection) => {
    return lodash
      .chain(collection)
      .groupBy((post) => DateTime.fromJSDate(post.date).year)
      .toPairs()
      .reverse()
      .value();
  });
  eleventyConfig.addFilter("groupByMonth", (collection) => {
    return lodash
      .chain(collection)
      .groupBy((post) => DateTime.fromJSDate(post.date).monthLong)
      .toPairs()
      .reverse()
      .value();
  });
  eleventyConfig.addFilter(
    "FrontMatterFilter",
    function (collection, property, value) {
      if (!property || !value) {
        return collection;
      }
      return collection.filter((item) => {
        if (
          item.hasOwnProperty("data") &&
          item.data.hasOwnProperty(property) &&
          item.data[property] == value
        ) {
          return true;
        }
      });
    },
  );
  eleventyConfig.addFilter(
    "hasFrontMatterProperty",
    function (collection, property) {
      if (!property) {
        return collection;
      }
      return collection.filter((item) => {
        if (
          item.hasOwnProperty("data") &&
          item.data.hasOwnProperty(property) &&
          item.data[property] !== "" &&
          item.data[property].length > 0
        ) {
          return true;
        }
      });
    },
  );

  eleventyConfig.addFilter(
    "urlStartsWith",
    function (collection, start, limit = 100, removeSelf = true) {
      if (!start) {
        return collection;
      }
      // Decrease by two to account for start and ending slash
      const startLength = start.split("/").length - 2;
      return collection.filter((item) => {
        if (removeSelf && item.url === start) {
          return false;
        }
        if (
          item.url.startsWith(start) &&
          Math.abs(item.url.split("/").length - 2 - startLength) <= limit
        ) {
          return true;
        }
      });
    },
  );

  eleventyConfig.addFilter("cssmin", function (code) {
    return new CleanCSS({ level: 2 }).minify(code).styles;
  });
  eleventyConfig.addFilter("date", function (date, format = "DD MMM YYYY") {
    return dayjs(date).format(format);
  });
  eleventyConfig.addFilter("inspect", function (value) {
    const str = util.inspect(value, true, 4, true);
    return `<div style="white-space: pre-wrap;">${decodeURI(str)}</div>`;
  });
  eleventyConfig.addFilter("console", function (value) {
    const str = util.inspect(value, true, 4, true);
    console.log(str);
    return "";
  });
  eleventyConfig.addShortcode("excerpt", function (article) {
    if (!article.hasOwnProperty("templateContent")) {
      return null;
    }
    const content = article.templateContent;
    if (content.length > 0) {
      return striptags(content)
        .substring(0, 200)
        .replace(/^\\s+|\\s+$|\\s+(?=\\s)/g, "")
        .trim()
        .concat("...");
    }
  });
  eleventyConfig.addNunjucksFilter("metaDescription", function (content) {
    if (content) {
      let paragraphContent = content.split("<p>");
      paragraphContent =
        paragraphContent.length > 2 ? paragraphContent[1].split("</p>") : null;
      paragraphContent = paragraphContent
        ? njkFilters.string(paragraphContent)
        : null;
      paragraphContent = paragraphContent
        ? njkFilters.striptags(paragraphContent)
        : null;
      paragraphContent = paragraphContent
        ? njkFilters.truncate(paragraphContent, 160)
        : null;
      return paragraphContent;
    }
    return null;
  });
  eleventyConfig.addShortcode("breadcrumbs", function (url) {
    if (
      url === undefined ||
      url == "" ||
      url.split("/") == false
      // url.split("/").filter((part) => part.length).length <= 1
    ) {
      return;
    }
    let parts = url.split("/").filter((part) => part.length);
    const pages = this.ctx.collections.pages;
    let links = `<li><a href="/">${metadata.title}</a></li>\n`;
    const URLs = [];

    for (var i = parts.length - 1; i >= 0; i--) {
      parts = parts.filter((v) => v !== parts[i]);
      if (parts.length > 0) {
        URLs.push(`/${parts.join("/")}/`);
      }
    }
    if (URLs.length > 0) {
      URLs.sort((a, b) => a.length - b.length);
      for (let i = 0; i < URLs.length; i++) {
        let page = pages.find((p) => p.url === URLs[i]);
        links += `<li><a href="${page.url}">${page.data.title}</a></li>\n`;
      }
    }
    return links;
  });
  eleventyConfig.addNunjucksShortcode("jsonld", function (type = "") {
    let data;
    try {
      data = matter(this.page.inputPath);
      data.url = this.page.url;
    } catch (error) {
      throw new Error(error);
    }
    let content;
    if (LinkedData.Types.WebPage.includes(type)) {
      content = LinkedData.WebPage({
        data: data,
        type: type,
      });
    }
    if (LinkedData.Types.Article.includes(type)) {
      content = LinkedData.Article({
        data: data,
        type: type,
      });
    }
    if (!lodash.isEmpty(content)) {
      try {
        return LinkedData.render([content]);
      } catch (error) {
        console.log(
          typeof content,
          "Array.isArray():",
          Array.isArray(content),
          "type:",
          type,
          " - ",
          this.page.url,
        );
        throw new Error(error);
      }
    }
  });

  // Customize Markdown library settings:
  eleventyConfig.setLibrary("md", markdownIt(MarkdownOptions));
  eleventyConfig.amendLibrary("md", (mdLib) => {
    // mdLib.use(markdownItAnchor, {
    //   permalink: markdownItAnchor.permalink.ariaHidden({
    //     placement: "after",
    //     class: "header-anchor",
    //     symbol: "#",
    //     ariaHidden: false,
    //   }),
    //   level: [1, 2, 3, 4],
    //   slugify: eleventyConfig.getFilter("slugify"),
    // });
    mdLib.use(markdownItEleventyImg, {
      // imgOptions: {
      // 	formats: ["avif", "webp", "jpeg", "jpg", "png"],
      // },
      // resolvePath: (filepath, env) =>
      // 	path.join(path.dirname(env.page.inputPath), filepath),
    });
    mdLib.use(markdownItFootnote);
  });

  eleventyConfig.addTransform("prettier", function (content, outputPath) {
    const extname = path.extname(outputPath);
    switch (extname) {
      case ".html":
        return prettier.format(content, { parser: "html" });
      case ".json":
        const parser = extname.replace(/^./, "");
        return prettier.format(content, { parser });
      default:
        return content;
    }
  });

  eleventyConfig.on("eleventy.after", () => {
    execSync(
      `npx pagefind --verbose --site _site --output-subdir pagefind --glob \"**/*.html\"`,
      {
        encoding: "utf-8",
      },
    );
  });

  // Features to make your build faster (when you need them)

  // If your passthrough copy gets heavy and cumbersome, add this line
  // to emulate the file copy on the dev server. Learn more:
  // https://www.11ty.dev/docs/copy/#emulate-passthrough-copy-during-serve

  // eleventyConfig.setServerPassthroughCopyBehavior("passthrough");

  return {
    // Control which files Eleventy will process
    // e.g.: *.md, *.njk, *.html, *.liquid
    templateFormats: ["md", "njk", "html", "liquid"],

    // Pre-process *.md files with: (default: `liquid`)
    markdownTemplateEngine: "njk",

    // Pre-process *.html files with: (default: `liquid`)
    htmlTemplateEngine: "njk",

    // These are all optional:
    dir: {
      input: "content", // default: "."
      includes: "../_includes", // default: "_includes"
      data: "../_data", // default: "_data"
      output: "_site",
    },

    // -----------------------------------------------------------------
    // Optional items:
    // -----------------------------------------------------------------

    // If your site deploys to a subdirectory, change `pathPrefix`.
    // Read more: https://www.11ty.dev/docs/config/#deploy-to-a-subdirectory-with-a-path-prefix

    // When paired with the HTML <base> plugin https://www.11ty.dev/docs/plugins/html-base/
    // it will transform any absolute URLs in your HTML to include this
    // folder name and does **not** affect where things go in the output folder.
    pathPrefix: "/",
  };
};
