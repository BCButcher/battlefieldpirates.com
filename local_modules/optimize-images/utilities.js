/**
 * Get process arguments
 * @returns object
 * @see https://stackoverflow.com/a/54098693
 */
function getArgs() {
  const args = {};
  process.argv.slice(2, process.argv.length).forEach((arg) => {
    if (arg.slice(0, 2) === "--") {
      const longArg = arg.split("=");
      const longArgFlag = longArg[0].slice(2, longArg[0].length);
      const longArgValue = longArg.length > 1 ? longArg[1] : true;
      if (typeof longArgValue === "string") {
        args[longArgFlag] = longArgValue.toLowerCase();
      } else {
        args[longArgFlag] = longArgValue;
      }
    } else if (arg[0] === "-") {
      const flags = arg.slice(1, arg.length).split("");
      flags.forEach((flag) => {
        args[flag] = true;
      });
    }
  });
  return args;
}

/**
 * Convert milliseconds to human readable time
 * @param {Number} millisec Float containing milliseconds
 *
 * @see https://stackoverflow.com/a/32180863
 */
function msToTime(millisec) {
  var milliseconds = millisec.toFixed(2);
  var seconds = (millisec / 1000).toFixed(1);
  var minutes = (millisec / (1000 * 60)).toFixed(1);
  var hours = (millisec / (1000 * 60 * 60)).toFixed(1);
  var days = (millisec / (1000 * 60 * 60 * 24)).toFixed(1);
  if (seconds <= 0) {
    return milliseconds + " ms";
  } else if (seconds < 60) {
    return seconds + " sec";
  } else if (minutes < 60) {
    return minutes + " min";
  } else if (hours < 24) {
    return hours + " hours";
  } else {
    return days + " days";
  }
}

/**
 * The percentual difference (in-/decrease) between two numbers divided by the original number.
 * @param {int} a First value
 * @param {int} b Second value
 * @param {int} round Round to give decimals
 * @returns {number} Percentage difference
 * @see https://stackoverflow.com/a/23760013
 */
function percentageChange(a, b, round = 0) {
  const value = (b / a) * 100 - 100;
  if (round > 0) {
    return value.toFixed(round);
  }
  return value;
}

/**
 * Trim leading and ending slashes
 * @param {string} str Input string
 * @returns {string} Trimmed string
 * @see https://stackoverflow.com/a/68146973
 */
function trimSlashes(str) {
  str = str.startsWith("/") ? str.substring(1) : str;
  str = str.endsWith("/") ? str.substring(0, str.length - 1) : str;
  return str;
}

module.exports = {
  getArgs: getArgs,
  msToTime: msToTime,
  percentageChange: percentageChange,
  trimSlashes: trimSlashes,
};
