const FS = require("fs"),
  child_process = require("child_process"),
  { performance } = require("perf_hooks"),
  path = require("path"),
  walker = require("klaw-sync"),
  sizeOf = require("image-size"),
  byteSize = require("byte-size"),
  chalk = require("chalk-cjs"),
  highlight = require("cli-highlight").highlight,
  jsdom = require("jsdom"),
  { JSDOM } = jsdom,
  { getArgs, msToTime, percentageChange, trimSlashes } = require("./utilities");
const args = getArgs();
let start = performance.now();
const MAGICK = path.resolve(
  "./bin/imagemagick-7.1.1-27-portable-Q16-x64/convert.exe",
);

console.log(chalk.magenta("OPTIMIZING IMAGES ..."));
let base = path.resolve("../../");
if (args.base) {
  base = path.resolve(args.base);
}
base = base.replace(/\\/g, "/");
if (args.debug) console.log(chalk.cyanBright("BASE"), base);
let targetFolder = "/_site/";
if (args.target) {
  targetFolder = `/_site/${args.target}`;
}
if (args.debug) console.log(chalk.cyanBright("TARGET"), targetFolder);

const imageFiles = walker(`${base}${targetFolder}`, {
  fs: FS,
  nodir: true,
  depthLimit: -1,
  traverseAll: true,
  filter: (item) => {
    return [".jpg", ".jpeg", ".png", ".webp"].includes(path.extname(item.path));
  },
});
if (args.debug)
  console.log(
    chalk.cyanBright("FOUND"),
    imageFiles.length,
    `Image(s) in ${targetFolder}`,
  );

let images = [];
let imagesSize = 0;
for (let i = 0; i < imageFiles.length; i++) {
  let filepath = imageFiles[i].path.replace(/\\/g, "/");
  images.push(filepath);
  imagesSize = imagesSize + FS.statSync(filepath).size;
}

let optimizedImages = 0,
  optimizedImagesSize = 0;
for (let i = 0; i < images.length; i++) {
  const target = images[i].replace(path.extname(images[i]), ".webp");
  if (FS.existsSync(target)) {
    continue;
  }
  child_process
    .execSync(
      `${MAGICK} "${images[i]}" -resize "1600>" -quality 80 "${target}"`,
      {
        timeout: 10000,
        stdio: "pipe",
      },
    )
    .toString();
  optimizedImages++;
  optimizedImagesSize = optimizedImagesSize + FS.statSync(target).size;
  if (args.debug)
    console.log(chalk.cyanBright("WROTE"), target.replace(base, ""));
  if (args.remove) FS.rmSync(images[i]);
}
console.log(
  chalk.magenta("OPTIMIZED"),
  chalk.whiteBright(optimizedImages),
  "Image(s) as WebP in",
  chalk.whiteBright(msToTime(performance.now() - start)),
);

if (optimizedImagesSize > 0)
  console.log(
    chalk.yellowBright(`${byteSize(imagesSize)}`),
    "=>",
    chalk.greenBright(`${byteSize(optimizedImagesSize)}`),
    `(${percentageChange(imagesSize, optimizedImagesSize, 2)}%)`,
  );

if (!args.html) return;

start = performance.now();
const HTMLFiles = walker(`${base}${targetFolder}`, {
  fs: FS,
  nodir: true,
  depthLimit: -1,
  traverseAll: true,
  filter: (item) => {
    return [".html"].includes(path.extname(item.path));
  },
});
if (args.debug)
  console.log(
    chalk.cyanBright("FOUND"),
    HTMLFiles.length,
    `HTML file(s) in ${targetFolder}`,
  );

for (let n = 0; n < HTMLFiles.length; n++) {
  let fileContent = FS.readFileSync(HTMLFiles[n].path, "utf8");
  const dom = new JSDOM(fileContent);
  const DOMImages = [...dom.window.document.querySelectorAll("img")];
  const HTMLFile = HTMLFiles[n].path
    .replace(/\\/g, "/")
    .replace(base, "")
    .replace(targetFolder, "");
  if (args.filter && HTMLFile !== args.filter) continue;
  if (args.debug)
    console.log(
      chalk.yellowBright(`/${HTMLFile}`),
      DOMImages.length,
      "Image(s)",
    );
  let LCPi = 0;
  for (let i = 0; i < DOMImages.length; i++) {
    if (
      DOMImages[i].src.startsWith("http://") ||
      DOMImages[i].src.startsWith("https://")
    ) {
      continue;
    }
    if (DOMImages[i].src.startsWith("data:")) {
      for (let a = 0; a < DOMImages[i].attributes.length; a++) {
        DOMImages[i].setAttribute(
          DOMImages[i].attributes[a].name,
          DOMImages[i].attributes[a].value.replace(/\.(jpe?g|png)/i, ".webp"),
        );
      }
      if (args.debug)
        console.log(highlight(DOMImages[i].outerHTML, { language: "html" }));
      continue;
    }
    DOMImages[i].src = DOMImages[i].src.replace(/\.(jpe?g|png)$/i, ".webp");
    const absoluteBase = trimSlashes(`${base}${targetFolder}`);
    let imagePath = "";
    if (DOMImages[i].src.startsWith("/")) {
      imagePath = path.resolve(`${absoluteBase}${DOMImages[i].src}`);
    } else {
      imagePath = path.resolve(
        `${absoluteBase}/${path.dirname(HTMLFile)}/${DOMImages[i].src}`,
      );
    }
    if (!FS.existsSync(imagePath)) continue;
    const dimensions = sizeOf(imagePath);
    DOMImages[i].setAttribute("width", dimensions.width);
    DOMImages[i].setAttribute("height", dimensions.height);
    DOMImages[i].setAttribute("loading", "lazy");
    if (LCPi == 0 && (dimensions.width >= 800 || dimensions.height >= 800)) {
      DOMImages[i].setAttribute("fetchpriority", "high");
      DOMImages[i].setAttribute("loading", "eager");
      LCPi = 1;
    }
    if (dimensions.width >= 800 || dimensions.height >= 800) {
      let LQIP = child_process
        .execSync(
          `${MAGICK} "${imagePath}" -quality 20 -resize 5x -strip inline:-"`,
          {
            timeout: 10000,
            stdio: "pipe",
          },
        )
        .toString();
      DOMImages[i].setAttribute(
        "style",
        `${DOMImages[i].style.cssText}background: url('${LQIP}');background-size: cover;`,
      );
    }
    if (args.debug)
      console.log(highlight(DOMImages[i].outerHTML, { language: "html" }));
  }
  const DOMMetaImages = [
    ...dom.window.document.querySelectorAll("meta[content$='.jpg']"),
  ];
  for (let i = 0; i < DOMMetaImages.length; i++) {
    DOMMetaImages[i].setAttribute(
      "content",
      DOMMetaImages[i].content.replace(".jpg", ".webp"),
    );
  }
  if (args.write) FS.writeFileSync(HTMLFiles[n].path, dom.serialize());
}

console.log(
  chalk.magenta("OPTIMIZED"),
  chalk.whiteBright(HTMLFiles.length),
  "HTML file(s) in",
  chalk.whiteBright(msToTime(performance.now() - start)),
);
