# Optimize Images

Reduces image weight by converting them to WebP, with default quality level, through ImageMagick. Only applied to JPG, JPEG, PNG, and WebP images whose longest side is 1920px or longer, or is more than 600 KB in size. The following arguments must be supplied:

`--base=""` - Relative path where images reside, typically `../../` to navigate to root directory
`--target=""` - Path to target folder where images reside, typically `img`. Hard-coded to include `/_site/` to avoid incorrectly changing source images

These arguments may be supplied:

`--remove` - Delete original images
`--write` - Overwrite original image references in HTML-files
`--debug` - Output additional information while processing

Example command ran from project root after `yarn build`:

`yarn workspace optimize-images optimize --base="../../" --target="img/game" --remove --write`
