const { registerFont, createCanvas, Image } = require("canvas");
const { drawText, textToWords } = require("text-to-canvas");
const cover = require("./cover");
const lodash = require("lodash");
registerFont("./fonts/OpenSans/OpenSans-VariableFont_wdth,wght.ttf", {
  family: "Open Sans",
});

/**
 * Composite image with background, watermark, and text.
 * @param {boolean} debug Debugging-mode
 * @param {string} backgroundImage Base64-encoded image
 * @param {string} watermarkImage Base64-encoded image
 * @param {string} title Text for main position
 * @param {string} author Text for bottom left position, if no watermark
 * @param {string} site Text for bottom right position
 * @param {int} width Alternate image width, if not default of 1200
 * @param {int} height Alternate image height, if not default of 630
 * @param {int} fontSize Alternate font size, if not default of 60
 * @returns {Buffer} Canvas as medium-quality JPG for storage
 */
const createImage = ({
  debug = null,
  background = "",
  watermark = "",
  title = "",
  subtitle = "",
  author = "",
  site = "",
  width = 1200,
  height = 630,
  fontSize = 72,
}) => {
  const canvas = createCanvas(width, height);
  const ctx = canvas.getContext("2d");
  ctx.imageSmoothingEnabled = true;

  // Draw base image
  const backgroundImage = new Image();
  backgroundImage.onload = () => ctx.drawImage(backgroundImage, 0, 0);
  backgroundImage.onerror = (err) => {
    console.error(err);
  };
  backgroundImage.src = background;
  cover(backgroundImage, 0, 0, width, height)
    .pan(0.5, 0.5)
    .zoom(1.25)
    .render(ctx);

  // Draw gradient overlay
  const gradient = ctx.createLinearGradient(0, 0, 0, 150);
  gradient.addColorStop(0, "rgba(0,0,0,0.15)");
  gradient.addColorStop(1, "rgba(0,0,0,0.35)");
  ctx.fillStyle = gradient;
  ctx.fillRect(0, 0, width, height);

  // Add shadows
  ctx.shadowColor = "#000000";
  ctx.shadowBlur = 3;
  ctx.shadowOffsetX = 2;
  ctx.shadowOffsetY = 2;

  // Draw the title(s) over multiple lines
  const defaults = {
    debug: debug,
    align: "left",
    vAlign: "top",
    width: 1080,
    font: "Open Sans",
    fontWeight: "bold",
    fontColor: "#ffffff",
    overflow: false,
  };
  let titleWords = textToWords(
    lodash.truncate(title, { length: 82, separator: " ", omission: " ..." }),
  );
  let words = titleWords;
  if (subtitle != null && subtitle !== "") {
    let subtitleWords = textToWords(
      lodash.truncate(subtitle, {
        length: 159,
        separator: " ",
        omission: " ...",
      }),
    );
    Object.keys(subtitleWords).forEach(function (key) {
      subtitleWords[key]["format"] = { fontSize: fontSize * 0.7 };
    });
    words = words.concat(
      [
        { text: "\n" },
        { text: " " },
        { text: "\n" },
        { text: " " },
        { text: "\n" },
      ],
      subtitleWords,
    );
  }
  drawText(
    ctx,
    words,
    lodash.assign(defaults, {
      x: 60,
      y: 70,
      height: 432,
      fontSize: fontSize,
      overflow: false,
    }),
  );

  // Draw watermark image or bottom left corner text
  if (watermark !== "") {
    const watermarkImage = new Image();
    watermarkImage.onload = () =>
      ctx.drawImage(watermarkImage, 60, 532, 64, 64);
    watermarkImage.onerror = (err) => {
      console.error(err);
    };
    watermarkImage.src = watermark;
  } else if (author !== "") {
    drawText(
      ctx,
      author,
      lodash.assign(defaults, {
        align: "left",
        vAlign: "middle",
        x: 60,
        y: 532,
        width: width * 0.4 - 60,
        height: 80,
        fontSize: fontSize / 2,
        overflow: false,
      }),
    );
  }

  // Draw bottom right corner text
  drawText(
    ctx,
    site,
    lodash.assign(defaults, {
      align: "right",
      vAlign: "middle",
      x: width * 0.4,
      y: 532,
      width: width * 0.6 - 60,
      height: 80,
      fontSize: fontSize / 2,
      overflow: false,
    }),
  );

  return canvas.toBuffer("image/jpeg", {
    quality: 0.75,
    progressive: true,
    chromaSubsampling: true,
  });
};

module.exports = {
  createImage,
};
