# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.0.6] - 2025-02-18

### Added

- Event footage from TheCalmingClam for BFP1

### Fixed

- Classes-page icon-size and alignment
- Upgrade Social image generation dependency

## [2.0.5] - 2024-12-29

### Added

- Event footage from Winbean for BFP1

### Fixed

- Header links color regression

## [2.0.4] - 2024-11-24

### Added

- Origin for 2024 BFP2 event

## [2.0.3] - 2024-11-05

### Added

- Gameplay images from Dead Mans Reef, Harbor, and Pressgang Port by Frosty
- Event footage from Spooky for BFP2

## [2.0.2] - 2024-10-27

### Added

- Aces High banner

### Fixed

- Uncharted Waters banner
- Typo and content in `/battlefield-pirates-2/about`
- Social image generation in `<meta>`

## [2.0.1] - 2024-10-08

### Fixed

- Favicon

## [2.0.0] - 2024-10-08

### Changed

- Separated `/galleries/footage/` into subpages, keeping known events with multiple videos at the top level
- HTML-rendering, loading, optimizations to improve performance and compliance
- Image optimization to ~83% size less than original
- Adjusted size of hero-image on home-layout

### Fixed

- "Page not found"-page
- Image links and duplicates
- WAI-ARIA issues
- Styling-issues
- More distinct link-styling and correct background-position in headers
- More explicit `role="link"`-styling for external links
- Update `browserlist` to better match browser's compatibility

### Added

- `exhibit`-template as a simplified display of gallery items, whereas the `gallery`-template is broader
- `local-web-server` for testing and benchmarking closer to production

## [1.3.2] - 2024-07-23

### Fixed

- Image optimization

## [1.3.1] - 2024-07-23

### Fixed

- Social image generation

### Added

- Description and origin-link to catalog-items

## [1.3.0] - 2024-07-23

### Added

- Enable individual audio-file description
- Version history in `/battlefield-pirates-2/about`

### Fixed

- Heading title on front page

### Changed

- More robust and simpler Social image generation
- Simplify image optimization

## [1.2.12] - 2024-06-22

### Added

- Event video footage from Laska for BFP1

## [1.2.11] - 2024-06-21

### Added

- Event video footage from Winbean and penske83 for BFP1

## [1.2.10] - 2024-06-09

### Added

- Event video footage from LastOfAvari for BFP1 and BFP2

## [1.2.9] - 2024-05-01

### Fixed

- Rebuild

## [1.2.8] - 2024-05-01

### Fixed

- Social Sharing Image

## [1.2.7] - 2024-04-28

### Fixed

- Table Of Contents styling
- Social Sharing Image

## [1.2.6] - 2024-04-28

### Added

- Event video footage from Winbean for BFP1

### Changed

- Allow nested structures in Gallery
- Nest events for 2024

## [1.2.5] - 2024-04-13

### Added

- Generate and apply social-sharing (OpenGraph) images
- Social-sharing image preview in footer

## [1.2.4] - 2024-02-24

### Added

- Event video footage from Winbean for BFP1

## [1.2.3] - 2024-01-23

### Fixed

- Social-sharing image

## [1.2.2] - 2024-01-23

### Added

- Event video footage and music video from LuccaWulf for BFP1
- Post-processing of images to optimize loading

### Fixed

- Banner-icon size
- Social-sharing image
- License
- Revert using Universally Unique IDentifier for gallery identifiers, use simpler, consistent method instead

## [1.2.1] - 2024-01-20

### Added

- Gameplay images from of Winbean
- Event video footage from Always Battlefield for BFP1, Winbean for BFP2

### Fixed

- Use Universally Unique IDentifier for gallery identifiers to avoid naming-conflicts
- Image-link styles

## [1.2.0] - 2024-01-19

### Added

- Event video footage from E3gaming for BFP1 and BFP2

### Fixed

- Nunjucks-macros
- Icon-references

## [1.1.0] - 2024-01-07

### Changed

- Repository name and location
- Define footer in metadata
- Made some elements more generic for reuse

### Added

- Section-Navigation macro
- Two-column template

### Fixed

- Home-link changed to site title
- Some inclusions in Nunjucks
- Search-template
- AnchorJS to Table Of Contents generation
- Links between pages
- Galleries

## [1.0.4] - 2023-12-27

### Added

- Lightbox to map images

### Fixed

- "Lost Soldiers" to "Lost-Soldiers"
- Description of Iron Seas in `/battlefield-pirates/about`
- Map paths and content

## [1.0.3] - 2023-12-21

### Added

- CONTRIBUTING.md

### Fixed

- License to MIT

## [1.0.2] - 2023-12-21

### Fixed

- Case-sensitive folder names

## [1.0.1] - 2023-12-21

### Added

- CHANGELOG.md
- QuickLinks

### Fixed

- Correct slugifying of tags
- Minified some static resources
- Issue and Merge templates for GitLab

## [1.0.0] - 2023-12-20

### New

- First public release
